﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.User
{
    public class AdminViewModel
    {
        public Data.User User { get; set; }
        public Data.Staff Staff { get; set; }
    }
}
