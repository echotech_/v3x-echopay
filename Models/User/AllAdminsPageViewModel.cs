﻿using EchoPay.Models.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.User
{
    public class AllAdminsPageViewModel
    {
        public CreateAdminCommand CreateAdminCommand { get; set; }
        public ICollection<AdminViewModel> AdminViewModels { get; set; }
        public ICollection<AdminViewModel> StaffViewModels { get; set; }
    }
}
