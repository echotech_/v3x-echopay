﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Staff
{
    public class AdminStaffPageViewModel
    {
        public CreateStaffCommand CreateStaffCommand { get; set; }
        public ICollection<StaffViewModel> StaffViewModels { get; set; }
    }
}
