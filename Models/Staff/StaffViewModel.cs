﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Models.Staff
{
    public class StaffViewModel
    {
        public int Id { get; set; }
        public Data.User User { get; set; }

        public string ICNo { get; set; }

        public string Name { get; set; }

        public string Position { get; set; }

        public string ContactNo { get; set; }

        // Address
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }

        public string Nationality { get; set; }

        public string Email { get; set; }

        public DateTime AdmissionDate { get; set; }

        public DateTime DOB { get; set; }

        public double SalaryAmount { get; set; }

        public string EPF_No { get; set; }

        public int EPF_StatusId { get; set; }

        // EPF_Rate
        public double EPF_EmployeeStaffRate { get; set; }
        public double EPF_EmployeeAdditionalRate { get; set; }
        public double EPF_EmployerRate { get; set; }

        public string SOCSO_Category { get; set; }
    }
}
