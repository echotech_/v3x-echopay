﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Staff
{
    public class AdminAttendancePageViewModel
    {
        public ICollection<StaffViewModel> StaffViewModels { get; set; }
        // others add later
    }
}
