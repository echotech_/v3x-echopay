﻿namespace EchoPay.Models.Staff
{
    public class CreateAttendanceCommand : AttendanceViewModel
    {
        // Id from AttendanceViewModel is not needed
        public Data.Attendance ToAttendance()
        {
            return new Data.Attendance
            {
                StaffId = StaffId,
                Date = Date,
                TimeIn = TimeIn,
                TimeOut = TimeOut
            };
        }
    }
}
