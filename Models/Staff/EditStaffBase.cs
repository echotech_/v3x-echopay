﻿using EchoPay.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Models.Staff
{
    public class EditStaffBase
    {
        public string UserId { get; set; }


        [Display(Name = "IC No./Passport No.")]
        public string ICNo { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Full Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Position")]
        public int PositionId { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string ContactNo { get; set; }

        // only used by service to process for database
        public int AddressId { get; set; }
        public Data.Address Address { get; set; }

        [Required]
        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        [Required]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "State")]
        public int StateId { get; set; }


        [Required]
        [Display(Name = "Nationality")]
        public int NationalityId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Admission Date")]
        public DateTime AdmissionDate { get; set; } = DateTime.Today;

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Birth")]
        public DateTime DOB { get; set; } = DateTime.Today;

        [Required]
        [Range(0, Double.PositiveInfinity)]
        [Display(Name = "Base Salary")]
        public double SalaryAmount { get; set; } = 1000;

        [Required]
        [Display(Name = "EPF No.")]
        public string EPF_No { get; set; }

        [Required]
        [Display(Name = "EPF Status")]
        public int EPF_StatusId { get; set; }

        [Required]
        [Display(Name = "Additional EPF Employer Contribution (e.g. 0.01 for 1%)")]
        public double EPF_EmployerRate { get; set; }

        [Required]
        [Display(Name = "Additional EPF Employee Contribution (e.g. 0.01 for 1%)")]
        public double EPF_EmployeeRate { get; set; }

        [Required]
        [Display(Name = "SOCSO Category")]
        public int SOCSO_CategoryId { get; set; }

        // select list
        public IEnumerable<SelectListItem> PositionList { get; set; }
        public IEnumerable<SelectListItem> StateList { get; set; }
        public IEnumerable<SelectListItem> NationalityList { get; set; }
        public IEnumerable<SelectListItem> EPF_StatusList { get; set; }
        public IEnumerable<SelectListItem> SOCSO_CategoryList { get; set; }
    }
}
