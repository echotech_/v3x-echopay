﻿using System.ComponentModel.DataAnnotations;

namespace EchoPay.Models.Staff
{
    public class EditPositionBase
    {
        [Required, StringLength(100)]
        [Display(Name = "New Position")]
        public string Name { get; set; }
    }
}
