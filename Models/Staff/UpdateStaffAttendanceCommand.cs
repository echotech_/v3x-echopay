﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Staff
{
    public class UpdateStaffAttendanceCommand
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        public int StaffId { get; set; }
        public string StaffName { get; set; }
        //public List<bool> SimpleAttendanceList { get; set; }
        public List<CreateAttendanceCommand> CreateAttendanceCommands { get; set; }
    }
}
