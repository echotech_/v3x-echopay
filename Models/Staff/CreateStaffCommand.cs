﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EchoPay.Models.Staff
{
    public class CreateStaffCommand : EditStaffBase
    {
        // send email to staff to set password
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        // 1 = send email, 2 = bind this account to self
        public int CreateStaffOption { get; set; } = 2;
        [Display(Name = "Choose your action")]
        public IEnumerable<SelectListItem> CreateStaffOptionList { get; set; }


        public Data.Staff ToStaff()
        {
            return new Data.Staff
            {
                ICNo = ICNo,
                UserId = UserId,
                Name = Name,
                PositionId = PositionId,
                ContactNo = ContactNo,
                Address = new Data.Address
                {
                    StreetAddress = StreetAddress,
                    City = City,
                    PostalCode = PostalCode,
                    StateId = StateId,
                },
                NationalityId = NationalityId,
                AdmissionDate = AdmissionDate,
                DOB = DOB,
                SalaryAmount = SalaryAmount,
                EPF_No = EPF_No,
                EPF_StatusId = EPF_StatusId,
                EPF_EmployerRate = EPF_EmployerRate,
                EPF_EmployeeRate = EPF_EmployeeRate,
                SOCSO_CategoryId = SOCSO_CategoryId
            };
        }
    }
}
