﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Models.Staff
{
    public class CreatePositionCommand : EditPositionBase
    {
        public Data.Position ToPosition()
        {
            return new Data.Position
            {
                Name = Name
            };
        }
    }
}
