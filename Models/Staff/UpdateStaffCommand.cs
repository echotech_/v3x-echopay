﻿using System.ComponentModel.DataAnnotations;

namespace EchoPay.Models.Staff
{
    public class UpdateStaffCommand : EditStaffBase
    {
        public int Id { get; set; }
        // send email to staff to set password
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }
        public void UpdateStaff(Data.Staff staff)
        {
            staff.ICNo = ICNo;
            staff.Name = Name;
            staff.PositionId = PositionId;
            staff.ContactNo = ContactNo;

            staff.Address.StreetAddress = StreetAddress;
            staff.Address.City = City;
            staff.Address.PostalCode = PostalCode;
            staff.Address.StateId = StateId;

            staff.NationalityId = NationalityId;
            staff.AdmissionDate = AdmissionDate;
            staff.DOB = DOB;
            staff.SalaryAmount = SalaryAmount;
            staff.EPF_No = EPF_No;
            staff.EPF_StatusId = EPF_StatusId;
            staff.EPF_EmployerRate = EPF_EmployerRate;
            staff.EPF_EmployeeRate = EPF_EmployeeRate;
            staff.SOCSO_CategoryId = SOCSO_CategoryId;
        }
    }
}
