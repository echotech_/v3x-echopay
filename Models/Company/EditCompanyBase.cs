﻿using EchoPay.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Models.Company
{
    public class EditCompanyBase
    {
        [Required]
        [StringLength(255)]
        [Display(Name = "Company Name")]
        public string Name { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Contact Number")]
        public string ContactNo { get; set; }

        [Required]
        public int AddressId { get; set; }

        public Data.Address Address { get; set; }

        [Required]
        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        [Required]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "State")]
        public int StateId { get; set; }


        [Required]
        [Display(Name = "Company Registration Number")]
        public string CompanyRegNo { get; set; }

        public int E_Number { get; set; }

        public int C_Number { get; set; }

        [Required]
        [Display(Name = "EPF Number")]
        public int EPF_No { get; set; }

        [Required]
        [Display(Name = "SOCSO Number")]
        public int SOCSO_No { get; set; }

        [Required]
        [Display(Name = "End of Month Payroll Day")]
        public int EndOfMonthPayrollDay { get; set; }

        [NotMapped]
        public IEnumerable<SelectListItem> StateList { get; set; }

    }
}
