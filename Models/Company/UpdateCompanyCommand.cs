﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Models.Company
{
    public class UpdateCompanyCommand:EditCompanyBase
    {
        public int Id { get; set; }

        public void UpdateCompany(Data.Company company)
        {
            company.Name = Name;
            company.Type = Type;
            company.ContactNo = ContactNo;

            company.Address.StreetAddress = StreetAddress;
            company.Address.City = City;
            company.Address.PostalCode = PostalCode;
            company.Address.StateId = StateId;

            company.CompanyRegNo = CompanyRegNo;
            company.E_Number = E_Number;
            company.C_Number = C_Number;
            company.EPF_No = EPF_No;
            company.SOCSO_No = SOCSO_No;
            company.EndOfMonthPayrollDay = EndOfMonthPayrollDay;
        }
    }
}
