﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Models.Company
{
    public class CreateCompanyCommand:EditCompanyBase
    {
        public Data.Company ToCompany()
        {
            return new Data.Company
            {
                Name = Name,
                Type = Type,
                ContactNo = ContactNo,
                Address = new Data.Address
                {
                    StreetAddress = StreetAddress,
                    City = City,
                    PostalCode = PostalCode,
                    StateId = StateId,
                },
                CompanyRegNo = CompanyRegNo,
                E_Number = E_Number,
                C_Number = C_Number,
                EPF_No = EPF_No,
                SOCSO_No = SOCSO_No,
                EndOfMonthPayrollDay = EndOfMonthPayrollDay
            };
        }
    }
}
