﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Models.Company
{
    public class CompanyViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ContactNo { get; set; }

        // address
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }


        public string CompanyRegNo { get; set; }
        public int E_Number { get; set; }
        public int C_Number { get; set; }
        public int EPF_No { get; set; }
        public int SOCSO_No { get; set; }
        public int EndOfMonthPayrollDay { get; set; }

    }
}
