﻿using EchoPay.Data;
using EchoPay.Models.Payslip;
using EchoPay.Models.Staff;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Others
{
    public class SettingsViewModel
    {
        public List<SelectListItem> PositionsList { get; set; }
        public List<PresetPayslipItem> PresetItemsList { get; set; }
        public CreatePositionCommand CreatePositionCommand { get; set; }
        public PresetPayslipItemViewModel PresetPayslipItemViewModel { get; set; }
        public List<EPFRatesViewModel> EPFRatesViewModelList { get; set; }
    }
}
