﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Others
{
    public class EPFRatesViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "From age")]
        public int AgeFrom { get; set; }

        [Required]
        [Display(Name = "EPF Status")]
        public int EPF_StatusId { get; set; }

        public string EPF_StatusString { get; set; }

        [Required]
        [Display(Name = "From salary")]
        public double SalaryRangeFrom { get; set; }

        [Required]
        [Display(Name = "Employer Rate, e.g. '0.13' for 13%; \n'5' for fixed amount of RM5")]
        public double EmployerRate { get; set; }

        [Required]
        [Display(Name = "Employee Rate, e.g. 0.07 for 7%")]
        public double EmployeeRate { get; set; }

    }
}
