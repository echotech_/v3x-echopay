﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Payslip
{
    public class ItemTuple
    {
        public int PresetItemId { get; set; }
        public double ItemAmount { get; set; }
    }
}
