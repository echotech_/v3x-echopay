﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Payslip
{
    public class PresetPayslipItemViewModel : Data.PresetPayslipItem
    {
        // select list
        public IEnumerable<SelectListItem> DirectionList { get; set; }
        public IEnumerable<SelectListItem> EPF_ApplicableList { get; set; }
        public IEnumerable<SelectListItem> SOCSO_ApplicableList { get; set; }
    }
}
