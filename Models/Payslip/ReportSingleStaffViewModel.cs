﻿using EchoPay.Models.Staff;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Payslip
{
    public class ReportSingleStaffViewModel
    {
        public StaffViewModel StaffViewModel { get; set; }
        public IEnumerable<SelectListItem> PastPayroll { get; set; }
        [Display(Name = "Select Year/Month")]
        public string SelectedPayroll { get; set; }
        public List<PayslipViewModel> PayslipViewModels { get; set; }
    }
}
