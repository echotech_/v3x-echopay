﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Payslip
{
    public class PayrollSummaryViewModel
    {
        public List<PayslipViewModel> PayslipViewModels { get; set; }
        public Data.Company Company { get; set; }
        public double TotalEmployeeNetPay { get; set; }
        public double TotalEPF_Payment { get; set; }
        public double TotalSOCSO_Payment { get; set; }
    }
}
