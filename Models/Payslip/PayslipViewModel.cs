﻿using EchoPay.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Models.Payslip
{
    public class PayslipViewModel
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public Data.Company Company { get; set; }
        public Data.Staff Staff { get; set; }
        public DateTime Date { get; set; }
        public double BaseSalary { get; set; }
        public double NetSalary { get; set; }
        public double InEPF_EmployerRate { get; set; }
        public double InEPF_EmployeeRate { get; set; }
        public double OutEPF_EmployerAmount { get; set; }
        public double OutEPF_EmployeeAmount { get; set; }
        public double OutSOCSO_EmployerAmount { get; set; }
        public double OutSOCSO_EmployeeAmount { get; set; }
        public List<PayslipItem> AddItems { get; set; }
        public List<PayslipItem> DeductItems { get; set; }
        public double VariablePay { get; set; }
        public double VariableDeduction { get; set; }

        public List<ItemTuple> ExistingItems { get; set; }

        // for user input
        [Display(Name = "New Item")]
        public int NewItemId { get; set; } = 0;

        [Display(Name = "Amount")]
        public double NewItemAmount { get; set; } = 0.0;

        // select list item
        public IEnumerable<SelectListItem> AvailablePayrollItems { get; set; }
    }
}
