﻿using EchoPay.Models.Company;
using EchoPay.Models.Staff;
using EchoPay.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace EchoPay.Models.SuperAdmin
{
    public class DashboardViewModel
    {
        public CompanyViewModel CompanyViewModel  { get; set; }
        public int StaffNumber { get; set; }
        public int AdminNumber { get; set; }
        public Dictionary<string, int> PositionNumbersDict { get; set; }
    }
}
