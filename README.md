# Payroll System for V3X
## Clone to local repo and restore packages
Open landing page of visual studio, click clone  

![alt text](https://i.imgur.com/3CCgs8A.png)

Then, enter the URL and choose your target path: https://gitlab.com/echotech_/v3x-echopay.git  

![alt text](https://i.imgur.com/tnGBXVo.png)

After cloning, restore the NuGet packages by opening the terminal  

![alt text](https://i.imgur.com/QK6A7xk.png)

Then enter command below to restore
```
dotnet restore
```

To use Git features, click the v3x_EchoPay besides "master" as shown:  

![alt text](https://i.imgur.com/2gSf2FA.png)

The panel appears, choose the project name.  

![alt text](https://i.imgur.com/jD0uA53.png)

Click Sync to synchronize with the remote(cloud) repository.  

![alt text](https://i.imgur.com/x6pqd6U.png)

To pull the Changes (updates from others) from the cloud (before starting to write your code), click Pull. There shouldn't be any problem if **everyone is working on different files**.  

![alt text](https://i.imgur.com/xdF4ExK.png)

After you have done your code, click Changes.  

![alt text](https://i.imgur.com/40LhHGE.png)

Enter a meaningful message and click commit all (and push if done; Push action can also be found in Sync section, below Pull).  

![alt text](https://i.imgur.com/Byf36nn.png)


## Database Migration
1. Make sure in app.settings.json at the project root, the DB connection string before the first semicolon (;) is correct. Initial Catalog (DB name) is not important as it will be created automatically. You can click _View > SQL Server Object Explorer_ to check.  

2. Open _Tools > NuGet Package Manager > Package Manager Console_, Enter  
	```
	Update-Database
	```

## Database Seeding (Initialization)
The data of the users with 3 roles will be seeded automatically when run, as found in _Startup.cs_ and _Data/ApplicationDbInitializer.cs.  

The test email and password is shown at the login page.

## Email For Testing
Account: echopay.noreply@gmail.com  

P/W: echo2u2i  

No phone number set. Free to test.

_note_: **less secure app access** is turned on in account setting to allow access from this system

## Theme of Views (Landing page and Dashboard)
This is the UI theme and kits used in this project. More component might be added later on.
### References:
- https://demos.creative-tim.com/material-dashboard/docs/2.1/getting-started/introduction.html
- https://demos.creative-tim.com/material-kit/docs/2.1/getting-started/introduction.html