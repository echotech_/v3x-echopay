﻿using EchoPay.Controllers;
using EchoPay.Data;
using EchoPay.Models.Staff;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Policy;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EchoPay.Models.SuperAdmin;

namespace EchoPay.Services
{
    public class StaffService
    {
        private readonly AppDbContext _context;
        private readonly PayslipItemService _payslipItemService;
        private readonly LocationService _locationService;
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public StaffService(
            AppDbContext context, 
            PayslipItemService payslipItemService,
            LocationService locationService,
            UserManager<User> userManager,
            IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _payslipItemService = payslipItemService;
            _locationService = locationService;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        // PAGE: ADMIN VIEW STAFF (complex model)
        public AdminStaffPageViewModel GetAdminStaffPageView()
        {
            SelectListItem[] staffOptionItems =
            {
                new SelectListItem
                {
                    Value = "1",
                    Text = "Send invitation email"
                }
            };
            List<SelectListItem> listStaffOptions = staffOptionItems.ToList();

            // if admin not bound as staff, show option to bind as staff
            string adminId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (_context.Staffs
                .Where(x => x.UserId == adminId)
                .FirstOrDefault() == null)
            {
                listStaffOptions.Add(new SelectListItem
                {
                    Value = "2",
                    Text = "This is me, bind to my admin account"
                });
            }
            
            CreateStaffCommand cmd = new CreateStaffCommand
            {
                PositionList = GetPositionsList(),
                EPF_StatusList = _payslipItemService.GetEPF_StatusList(),
                SOCSO_CategoryList = _payslipItemService.GetSOCSO_CategoryList(),
                NationalityList = _locationService.GetNationsList(),
                StateList = _locationService.GetStatesList(),
                NationalityId = _locationService.GetIdOfMalaysiaInNations(),
                CreateStaffOptionList = listStaffOptions
            };

            return new AdminStaffPageViewModel
            {
                StaffViewModels = GetStaffs(),
                CreateStaffCommand = cmd
            };
        }

        // PAGE: ADMIN VIEW ATTENDANCE (complex model)
        public AdminAttendancePageViewModel GetAdminAttendancePageView()
        {

            return new AdminAttendancePageViewModel
            {
                StaffViewModels = GetStaffs()
            };
        }

        public UpdateStaffAttendanceCommand GetStaffForAttendance(int id)
        {
            // exception!!
            // remember to add a <= b!
            var startDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            var endDate = DateTime.Today;
            List<CreateAttendanceCommand> allAttendances = new List<CreateAttendanceCommand>();
            var attended = GetAttendancesByStaff(id);
            Queue<CreateAttendanceCommand> onlyAttended = new Queue<CreateAttendanceCommand>(attended);

            for (DateTime d = startDate; d <= endDate; d = d.AddDays(1))
            {
                if (onlyAttended.Count > 0 && onlyAttended.Peek().Date.Date == d.Date)
                {
                    allAttendances.Add(onlyAttended.Dequeue());
                }
                else
                {
                    // add empty attendance
                    allAttendances.Add(new CreateAttendanceCommand
                    {
                        StaffId = id,
                        Date = d.Date
                    });
                }
            }

            return new UpdateStaffAttendanceCommand
            {
                StartDate = startDate,
                EndDate = endDate,
                StaffId = id,
                CreateAttendanceCommands = allAttendances,
                StaffName = _context.Staffs.Find(id).Name
            };
        }

        public UpdateStaffAttendanceCommand GetStaffForAttendance(int id, DateTime start, DateTime end)
        {
            var startDate = start;
            var endDate = end;
            List<CreateAttendanceCommand> allAttendances = new List<CreateAttendanceCommand>();
            Queue<CreateAttendanceCommand> onlyAttended = new Queue<CreateAttendanceCommand>(GetAttendancesByStaff(id));

            for (DateTime d = startDate; d <= endDate; d = d.AddDays(1))
            {
                if (onlyAttended.Count > 0 && onlyAttended.Peek().Date.Date == d.Date)
                {
                    allAttendances.Add(onlyAttended.Dequeue());
                }
                else
                {
                    // add empty attendance
                    allAttendances.Add(new CreateAttendanceCommand
                    {
                        StaffId = id,
                        Date = d.Date
                    });
                }
            }

            return new UpdateStaffAttendanceCommand
            {
                StartDate = startDate,
                EndDate = endDate,
                StaffId = id,
                CreateAttendanceCommands = allAttendances
            };
        }

        // remove attendance
        public void RemoveAttendance(CreateAttendanceCommand cmd)
        {
            var att = _context.Attendances.Find(cmd.Id);
            _context.Attendances.Remove(att);
            _context.SaveChanges();
        }

        // READ
        //
        //

        // get position list in selectlists
        public IEnumerable<SelectListItem> GetPositionsList()
        {
            return _context.Positions
                .Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Name
                });
        }

        // get a list of all staffs
        public ICollection<StaffViewModel> GetStaffs()
        {
            return _context.Staffs
                .Select(x => new StaffViewModel
                {
                    Id = x.Id,
                    User = x.User,
                    ICNo = x.ICNo,
                    Name = x.Name,
                    Position = x.Position.Name,
                    ContactNo = x.ContactNo,
                    StreetAddress = x.Address.StreetAddress,
                    City = x.Address.City,
                    PostalCode = x.Address.PostalCode,
                    State = x.Address.State.Name,
                    Nationality = x.Nationality.Name,
                    Email = x.User.Email,
                    AdmissionDate = x.AdmissionDate,
                    DOB = x.DOB,
                    SalaryAmount = x.SalaryAmount,
                    EPF_No = x.EPF_No,
                    EPF_StatusId = x.EPF_StatusId,
                    SOCSO_Category = x.SOCSO_Category.Scheme
                })
                .ToList();
        }

        // get single staff data
        public StaffViewModel GetOneStaff(int id)
        {
            return _context.Staffs
                .Where(x => x.Id == id)
                .Select(x => new StaffViewModel
                {
                    Id = x.Id,
                    User = x.User,
                    ICNo = x.ICNo,
                    Name = x.Name,
                    Position = x.Position.Name,
                    ContactNo = x.ContactNo,
                    StreetAddress = x.Address.StreetAddress,
                    City = x.Address.City,
                    PostalCode = x.Address.PostalCode,
                    State = x.Address.State.Name,
                    Nationality = x.Nationality.Name,
                    Email = x.User.Email,
                    AdmissionDate = x.AdmissionDate,
                    DOB = x.DOB,
                    SalaryAmount = x.SalaryAmount,
                    EPF_No = x.EPF_No,
                    EPF_StatusId = x.EPF_StatusId,
                    SOCSO_Category = x.SOCSO_Category.Scheme
                })
                .SingleOrDefault();
        }

        public Staff GetStaffById(int id)
        {
            return _context.Staffs
                .Where(x => x.Id == id)
                .Include(x => x.User)
                .FirstOrDefault();
        }

        // get single staff for update
        public UpdateStaffCommand GetStaffForUpdate(int id)
        {
            return _context.Staffs
                .Where(x => x.Id == id)
                .Select(x => new UpdateStaffCommand
                {
                    Id = x.Id,
                    UserId = x.User.Id,
                    ICNo = x.ICNo,
                    Name = x.Name,
                    Email = x.User.Email,
                    PositionId = x.PositionId,
                    ContactNo = x.ContactNo,
                    AddressId = x.AddressId,
                    Address = x.Address,
                    StreetAddress = x.Address.StreetAddress,
                    City = x.Address.City,
                    PostalCode = x.Address.PostalCode,
                    StateId = x.Address.StateId,
                    NationalityId = x.NationalityId,
                    AdmissionDate = x.AdmissionDate,
                    DOB = x.DOB,
                    SalaryAmount = x.SalaryAmount,
                    EPF_No = x.EPF_No,
                    EPF_StatusId = x.EPF_StatusId,
                    EPF_EmployerRate = x.EPF_EmployerRate,
                    EPF_EmployeeRate = x.EPF_EmployeeRate,
                    SOCSO_CategoryId = x.SOCSO_CategoryId,

                    PositionList = GetPositionsList(),
                    EPF_StatusList = _payslipItemService.GetEPF_StatusList(),
                    SOCSO_CategoryList = _payslipItemService.GetSOCSO_CategoryList(),
                    NationalityList = _locationService.GetNationsList(),
                    StateList = _locationService.GetStatesList(),
                })
                .SingleOrDefault();
        }

        // get attendances by date
        public ICollection<CreateAttendanceCommand> GetAttendancesByDate(DateTime dateTime)
        {
            return _context.Attendances
                .Where(x => x.Date.Date == dateTime.Date)
                .Select(x => new CreateAttendanceCommand
                {
                    StaffId = x.StaffId,
                    StaffName = x.Staff.Name,
                    Date = x.Date.Date,
                    TimeIn = x.TimeIn,
                    TimeOut = x.TimeOut,
                    AttendedFlag = 1
                })
                .ToList();
        }

        // get attendances by staff
        public ICollection<CreateAttendanceCommand> GetAttendancesByStaff(int id)
        {
            return _context.Attendances
                .Where(x => x.StaffId == id)
                .Select(x => new CreateAttendanceCommand
                {
                    Id = x.Id,
                    StaffId = x.StaffId,
                    StaffName = x.Staff.Name,
                    Date = x.Date.Date,
                    TimeIn = x.TimeIn,
                    TimeOut = x.TimeOut,
                    AttendedFlag = 1
                })
                .OrderBy(x => x.Date)
                .ToList();
        }



        // CREATE
        //
        //
        // create new staff
        public Staff CreateStaff(CreateStaffCommand cmd)
        {
            // first create new address from the attributes
            var staff = cmd.ToStaff();
            // new Address data is added too, with foreign key autolinked
            _context.Add(staff);
            _context.SaveChanges();
            return staff;
        }


        public async Task<bool> CreateStaffAndBindToAdminAsync(CreateStaffCommand cmd)
        {
            var staff = CreateStaff(cmd);
            staff.UserId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            staff.User = await _userManager.FindByIdAsync(staff.UserId);
            // new email for you, admin!
            staff.User.Email = cmd.Email;
            _context.SaveChanges();

            return true;
        }



        // create new attendance
        public void CreateAttendance(CreateAttendanceCommand cmd)
        {
            var attendance = cmd.ToAttendance();
            attendance.TimeIn = attendance.Date.Date;
            _context.Add(attendance);
            _context.SaveChanges();
        }




        // UPDATE
        //
        //
        // update staff
        public void UpdateStaff(UpdateStaffCommand cmd)
        {
            var staff = _context.Staffs.Find(cmd.Id);
            if (staff == null)
            {
                throw new Exception("Data not found");
            }

            staff.Address = _context.Addresses.Find(staff.AddressId);

            cmd.UpdateStaff(staff);
            _context.SaveChanges();
        }

        // Create New Position
        public void CreatePosition(CreatePositionCommand cmd)
        {
            _context.Positions.Add(cmd.ToPosition());
            _context.SaveChanges();
        }

    }
}
