﻿using EchoPay.Data;
using EchoPay.Models.Staff;
using EchoPay.Models.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EchoPay.Services
{
    public class UserService
    {
        private readonly AppDbContext _context;
        private readonly UserManager<User> _userManager;

        public UserService(
            AppDbContext context,
            UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // PAGE: SUPERADMIN VIEW + ADD ADMIN (complex model)
        public async Task<AllAdminsPageViewModel> GetAllAdminsPageViewAsync()
        {
            // ge admins first
            var result = await _userManager.GetUsersForClaimAsync(
                new Claim(ClaimTypes.Role, "ADMIN"));

            var adminList = result.ToList();
            var adminViewModels = new List<AdminViewModel>();
            foreach (var item in adminList)
            {
                var tempStaff = _context.Staffs
                        .Where(x => x.UserId == item.Id)
                        .Select(x => x)
                        .FirstOrDefault();

                if (tempStaff != null)
                {
                    tempStaff.Position = _context.Positions.Where(x => x.Id == tempStaff.PositionId).FirstOrDefault();
                }

                adminViewModels.Add(new AdminViewModel
                {
                    User = item,
                    Staff = tempStaff
                });
            }

            // then get non admin
            result = await _userManager.GetUsersForClaimAsync(
                new Claim(ClaimTypes.Role, "STAFF"));

            var staffList = result.ToList();
            var staffViewModels = new List<AdminViewModel>();
            foreach (var item in staffList)
            {
                var tempStaff = _context.Staffs
                        .Where(x => x.UserId == item.Id)
                        .Select(x => x)
                        .FirstOrDefault();

                if (tempStaff != null)
                {
                    tempStaff.Position = _context.Positions.Where(x => x.Id == tempStaff.PositionId).FirstOrDefault();
                }

                staffViewModels.Add(new AdminViewModel
                {
                    User = item,
                    Staff = tempStaff
                });
            }

            return new AllAdminsPageViewModel
            {
                CreateAdminCommand = new CreateAdminCommand(),
                AdminViewModels = adminViewModels,
                StaffViewModels = staffViewModels
            };
        }

        public async Task SetAsAdmin(string id)
        {
            var staff = await _userManager.FindByIdAsync(id);
            var staffClaims = await _userManager.GetClaimsAsync(staff);
            var oldClaim = staffClaims.Where(x => x.Type.Equals(ClaimTypes.Role)).FirstOrDefault();
            if (oldClaim != null)
            {
                await _userManager.RemoveClaimAsync(staff, oldClaim);
            }

            // raise rank to admin
            var newClaim = new Claim(ClaimTypes.Role, "ADMIN");
            _userManager.AddClaimAsync(staff, newClaim).Wait();
        }

        public async Task<bool> CreateAdmin(CreateAdminCommand cmd)
        {
            var user = new User
            {
                UserName = cmd.UserName,
                Email = ""
            };

            var result = await _userManager.CreateAsync(user, cmd.NewPassword);
            if (result.Succeeded)
            {
                var newClaim = new Claim(ClaimTypes.Role, "ADMIN");
                _userManager.AddClaimAsync(user, newClaim).Wait();
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task TerminateAdminAsync(string id)
        {
            var admin = await _userManager.FindByIdAsync(id);
            var adminClaims = await _userManager.GetClaimsAsync(admin);
            var oldClaim = adminClaims.Where(x => x.Type.Equals(ClaimTypes.Role)).FirstOrDefault();
            if (oldClaim != null)
            {
                await _userManager.RemoveClaimAsync(admin, oldClaim);
            }

            // lower rank to staff
            var newClaim = new Claim(ClaimTypes.Role, "STAFF");
            _userManager.AddClaimAsync(admin, newClaim).Wait();
        }
    }
}
