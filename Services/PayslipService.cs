﻿using EchoPay.Data;
using EchoPay.Models;
using EchoPay.Models.Payslip;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Org.BouncyCastle.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Services
{
    public class PayslipService
    {
        // NOTE:
        // the EPF rate should be updated so that base salary + bonus/etc. if exceed threshold, it will
        // use next rate
        //

        private readonly AppDbContext _context;
        private readonly PayslipItemService _payslipItemService;
        private readonly StaffService _staffService;

        public PayslipService(AppDbContext context, PayslipItemService payslipItemService, StaffService staffService)
        {
            _context = context;
            _payslipItemService = payslipItemService;
            _staffService = staffService;
        }

        public bool CheckThisMonthSubmitted()
        {
            var now = DateTime.Now;

            return _context.Payslips
                .Where(x => x.Date.Year == now.Year)
                .Where(x => x.Date.Month == now.Month)
                .FirstOrDefault()
                != null;
        }

        public List<PayslipViewModel> GetPayslipByStaff(int id)
        {
            var company = _context.Companies.Include(x => x.Address).Include(x => x.Address.State).FirstOrDefault();
            var foundPayslips = _context.Payslips
                .Where(x => x.StaffId == id)
                .Include(x => x.Staff)
                .Include(x => x.Staff.Position)
                .Include(x => x.Staff.Address)
                .Include(x => x.Staff.Address.State)
                .Include(x => x.SOCSO_Category)
                .Include(x => x.EPF_Status)
                .Include(x => x.PayslipItems);

            if (foundPayslips.Count() == 0)
            {
                return null;
            }

            var payslipsData = foundPayslips
                .Select(x => new PayslipViewModel
                {
                    StaffId = x.StaffId,
                    Staff = x.Staff,
                    BaseSalary = x.BaseSalary,
                    NetSalary = x.NetSalary,
                    VariablePay = x.VariablePay,
                    VariableDeduction = x.VariableDeduction,
                    Date = x.Date,
                    InEPF_EmployeeRate = x.InEPF_EmployeeRate,
                    InEPF_EmployerRate = x.InEPF_EmployerRate,
                    OutEPF_EmployerAmount = x.OutEPF_EmployerAmount,
                    OutEPF_EmployeeAmount = x.OutEPF_EmployeeAmount,
                    OutSOCSO_EmployeeAmount = x.OutSOCSO_EmployeeAmount,
                    OutSOCSO_EmployerAmount = x.OutSOCSO_EmployerAmount,
                    Id = x.Id,
                    Company = company,
                    AddItems = new List<PayslipItem>(),
                    DeductItems = new List<PayslipItem>()
                })
                .ToList();


            foreach (var result in payslipsData)
            {
                var allItems = _context.PayslipItems.Where(i => i.PayslipId == result.Id).ToList();
                foreach (var item in allItems)
                {
                    var preset = _context.PresetPayslipItems.Find(item.PresetPayslipItemId);
                    item.PresetPayslipItem = preset;
                    if (preset.Direction > 0)
                    {
                        result.AddItems.Add(item);
                    }
                    else
                    {
                        result.DeductItems.Add(item);
                    }
                }
            }

            return payslipsData;
        }

        public ReportSingleStaffViewModel GetSingleStaffReport(int id)
        {
            return new ReportSingleStaffViewModel
            {
                PastPayroll = GetPastPayrollList(),
                PayslipViewModels = GetPayslipByStaff(id),
                StaffViewModel = _staffService.GetOneStaff(id)
            };
        }

        // get
        public List<PayslipViewModel> GetNewPayrollComplexModel()
        {
            if (_context.Staffs.Count() == 0)
            {
                return null;
            }
            var presets = _payslipItemService.GetPresetItems();
            var modelList = _context.Staffs
                .Where(x => !x.IsArchived)
                .Select(x => new PayslipViewModel
                {
                    Staff = x,
                    StaffId = x.Id,
                    Date = DateTime.Now,
                    BaseSalary = x.SalaryAmount,
                    NetSalary = x.SalaryAmount,
                    InEPF_EmployerRate = x.EPF_EmployerRate,
                    InEPF_EmployeeRate = x.EPF_EmployeeRate,
                    AddItems = new List<PayslipItem>(),
                    DeductItems = new List<PayslipItem>(),
                    AvailablePayrollItems = presets,
                    ExistingItems = new List<ItemTuple>()
                })
                .ToList();

            foreach (var x in modelList)
            {
                x.Staff.Position = _context.Positions.Find(x.Staff.PositionId);
            }
            return modelList;
        }

        // for testing only
        public List<PayslipViewModel> SeedPayrollComplexModel(DateTime dateTime)
        {
            if (_context.Staffs.Count() == 0)
            {
                return null;
            }
            var presets = _payslipItemService.GetPresetItems();
            var modelList = _context.Staffs
                .Where(x => !x.IsArchived)
                .Select(x => new PayslipViewModel
                {
                    Staff = x,
                    StaffId = x.Id,
                    Date = dateTime,
                    BaseSalary = x.SalaryAmount,
                    NetSalary = x.SalaryAmount,
                    InEPF_EmployerRate = x.EPF_EmployerRate,
                    InEPF_EmployeeRate = x.EPF_EmployeeRate,
                    AddItems = new List<PayslipItem>(),
                    DeductItems = new List<PayslipItem>(),
                    AvailablePayrollItems = presets,
                    ExistingItems = new List<ItemTuple>()
                })
                .ToList();

            foreach (var x in modelList)
            {
                x.Staff.Position = _context.Positions.Find(x.Staff.PositionId);
            }
            return modelList;
        }

        public PayrollHistoryViewModel GetLatestPayroll()
        {
            var company = _context.Companies
                .Include(x => x.Address)
                .Include(x => x.Address.State)
                .FirstOrDefault();

            var latestPayslip = _context.Payslips.OrderByDescending(x => x.Date).FirstOrDefault();

            if (latestPayslip == null)
            {
                return null;
            }

            var latestYear = latestPayslip.Date.Year;
            var latestMonth = latestPayslip.Date.Month;

            var payslipsData = _context.Payslips
                .Where(x => x.Date.Year == latestYear && x.Date.Month == latestMonth)
                .Include(x => x.Staff)
                .Include(x => x.Staff.Address)
                .Include(x => x.Staff.Position)
                .Include(x => x.Staff.EPF_Status)
                .Include(x => x.Staff.SOCSO_Category)
                .Select(x => new PayslipViewModel 
                { 
                    StaffId = x.StaffId,
                    Staff = x.Staff,
                    BaseSalary = x.BaseSalary,
                    NetSalary = x.NetSalary,
                    VariablePay = x.VariablePay,
                    VariableDeduction = x.VariableDeduction,
                    Date = x.Date,
                    InEPF_EmployeeRate = x.InEPF_EmployeeRate,
                    InEPF_EmployerRate = x.InEPF_EmployerRate,
                    OutEPF_EmployerAmount = x.OutEPF_EmployerAmount,
                    OutEPF_EmployeeAmount = x.OutEPF_EmployeeAmount,
                    OutSOCSO_EmployeeAmount = x.OutSOCSO_EmployeeAmount,
                    OutSOCSO_EmployerAmount = x.OutSOCSO_EmployerAmount,
                    Id = x.Id,
                    Company = company,
                    AddItems = new List<PayslipItem>(),
                    DeductItems = new List<PayslipItem>()
                })
                .ToList();

            double totalEmployeeNetPay = 0.0;
            double totalEPF_Payment = 0.0;
            double totalSOCSO_Payment = 0.0;

            foreach (var result in payslipsData)
            {
                var allItems = _context.PayslipItems.Where(i => i.PayslipId == result.Id).ToList();
                foreach (var item in allItems)
                {
                    var preset = _context.PresetPayslipItems.Find(item.PresetPayslipItemId);
                    item.PresetPayslipItem = preset;
                    if (preset.Direction > 0)
                    {
                        result.AddItems.Add(item);
                    }
                    else
                    {
                        result.DeductItems.Add(item);
                    }
                }

                totalEmployeeNetPay += result.NetSalary;
                totalEPF_Payment += result.OutEPF_EmployeeAmount + result.OutEPF_EmployerAmount;
                totalSOCSO_Payment += result.OutSOCSO_EmployeeAmount + result.OutSOCSO_EmployerAmount;
            }

            var summary =  new PayrollSummaryViewModel
            {
                Company = company,
                TotalEmployeeNetPay = totalEmployeeNetPay,
                TotalEPF_Payment = totalEPF_Payment,
                TotalSOCSO_Payment = totalSOCSO_Payment,
                PayslipViewModels = payslipsData
            };

            var listOfHtml = new List<string>();
            var listOfStaffId = new List<int>();
            for (int i = 0; i < summary.PayslipViewModels.Count; i++)
            {
                listOfHtml.Add("");
                listOfStaffId.Add(0);
            }

            return new PayrollHistoryViewModel
            {
                PayrollSummaryViewModel = summary,
                PastPayroll = GetPastPayrollList(),
                PayslipHTML = listOfHtml,
                PayslipStaffSequence = listOfStaffId
            };
        }


        public PayrollHistoryViewModel GetPayrollByDateString(string dateString)
        {
            var convertedDate = DateTime.Parse(dateString);
            var company = _context.Companies
                .Include(x => x.Address)
                .Include(x => x.Address.State)
                .FirstOrDefault();

            var payslipsData = _context.Payslips
                .Where(x => x.Date.Year == convertedDate.Year && x.Date.Month == convertedDate.Month)
                .Include(x => x.Staff)
                .Include(x => x.Staff.Address)
                .Include(x => x.Staff.Position)
                .Include(x => x.Staff.EPF_Status)
                .Include(x => x.Staff.SOCSO_Category)
                .Select(x => new PayslipViewModel
                {
                    StaffId = x.StaffId,
                    Staff = x.Staff,
                    BaseSalary = x.BaseSalary,
                    NetSalary = x.NetSalary,
                    VariablePay = x.VariablePay,
                    VariableDeduction = x.VariableDeduction,
                    Date = x.Date,
                    InEPF_EmployeeRate = x.InEPF_EmployeeRate,
                    InEPF_EmployerRate = x.InEPF_EmployerRate,
                    OutEPF_EmployerAmount = x.OutEPF_EmployerAmount,
                    OutEPF_EmployeeAmount = x.OutEPF_EmployeeAmount,
                    OutSOCSO_EmployeeAmount = x.OutSOCSO_EmployeeAmount,
                    OutSOCSO_EmployerAmount = x.OutSOCSO_EmployerAmount,
                    Id = x.Id,
                    Company = company,
                    AddItems = new List<PayslipItem>(),
                    DeductItems = new List<PayslipItem>()
                })
                .ToList();

            double totalEmployeeNetPay = 0.0;
            double totalEPF_Payment = 0.0;
            double totalSOCSO_Payment = 0.0;

            foreach (var result in payslipsData)
            {
                var allItems = _context.PayslipItems.Where(i => i.PayslipId == result.Id).ToList();
                foreach (var item in allItems)
                {
                    var preset = _context.PresetPayslipItems.Find(item.PresetPayslipItemId);
                    item.PresetPayslipItem = preset;
                    if (preset.Direction > 0)
                    {
                        result.AddItems.Add(item);
                    }
                    else
                    {
                        result.DeductItems.Add(item);
                    }
                }

                totalEmployeeNetPay += result.NetSalary;
                totalEPF_Payment += result.OutEPF_EmployeeAmount + result.OutEPF_EmployerAmount;
                totalSOCSO_Payment += result.OutSOCSO_EmployeeAmount + result.OutSOCSO_EmployerAmount;
            }

            var summary = new PayrollSummaryViewModel
            {
                Company = company,
                TotalEmployeeNetPay = totalEmployeeNetPay,
                TotalEPF_Payment = totalEPF_Payment,
                TotalSOCSO_Payment = totalSOCSO_Payment,
                PayslipViewModels = payslipsData
            };

            // get all payroll month and year
            var allPayrollList = _context.Payslips
                .GroupBy(O => new { O.Date.Year, O.Date.Month })
                .Select(x => new SelectListItem
                {
                    Text = x.Key.Year.ToString() + "/" + x.Key.Month.ToString().PadLeft(2, '0'),
                    Value = new DateTime(x.Key.Year, x.Key.Month, 1).ToString()
                })
                .ToList();

            var listOfHtml = new List<string>();
            var listOfStaffId = new List<int>();
            for (int i = 0; i < summary.PayslipViewModels.Count; i++)
            {
                listOfHtml.Add("");
                listOfStaffId.Add(0);
            }

            return new PayrollHistoryViewModel
            {
                PayrollSummaryViewModel = summary,
                PastPayroll = allPayrollList,
                PayslipHTML = listOfHtml,
                PayslipStaffSequence = listOfStaffId
            };
        }

        public List<PayrollHistoryViewModel> GetAllPayroll()
        {
            List<PayrollHistoryViewModel> allPayroll = new List<PayrollHistoryViewModel>();

            var company = _context.Companies
                .Include(x => x.Address)
                .Include(x => x.Address.State)
                .FirstOrDefault();

            var payslipsData = _context.Payslips
                .Include(x => x.Staff)
                .Include(x => x.Staff.Address)
                .Include(x => x.Staff.Position)
                .Include(x => x.Staff.EPF_Status)
                .Include(x => x.Staff.SOCSO_Category)
                .OrderByDescending(x => x.Date)
                .Select(x => new PayslipViewModel
                {
                    StaffId = x.StaffId,
                    Staff = x.Staff,
                    BaseSalary = x.BaseSalary,
                    NetSalary = x.NetSalary,
                    VariablePay = x.VariablePay,
                    VariableDeduction = x.VariableDeduction,
                    Date = x.Date,
                    InEPF_EmployeeRate = x.InEPF_EmployeeRate,
                    InEPF_EmployerRate = x.InEPF_EmployerRate,
                    OutEPF_EmployerAmount = x.OutEPF_EmployerAmount,
                    OutEPF_EmployeeAmount = x.OutEPF_EmployeeAmount,
                    OutSOCSO_EmployeeAmount = x.OutSOCSO_EmployeeAmount,
                    OutSOCSO_EmployerAmount = x.OutSOCSO_EmployerAmount,
                    Id = x.Id,
                    Company = company,
                    AddItems = new List<PayslipItem>(),
                    DeductItems = new List<PayslipItem>()
                })
                .ToList();


            // group by date
            List<List<PayslipViewModel>> array = new List<List<PayslipViewModel>>();

            int tempYear = -1, tempMonth = -1;
            int cursor = -1;

            foreach (var item in payslipsData)  // payslipsData already sorted by date
            {
                if (item.Date.Year != tempYear || item.Date.Month != tempMonth)
                {
                    tempYear = item.Date.Year;
                    tempMonth = item.Date.Month;
                    array.Add(new List<PayslipViewModel>());
                    ++cursor;
                }

                array.ElementAt(cursor).Add(item);
            }

            foreach (var data in array)
            {
                double totalEmployeeNetPay = 0.0;
                double totalEPF_Payment = 0.0;
                double totalSOCSO_Payment = 0.0;

                foreach (var result in data)
                {
                    var allItems = _context.PayslipItems.Where(i => i.PayslipId == result.Id).ToList();
                    foreach (var item in allItems)
                    {
                        var preset = _context.PresetPayslipItems.Find(item.PresetPayslipItemId);
                        item.PresetPayslipItem = preset;
                        if (preset.Direction > 0)
                        {
                            result.AddItems.Add(item);
                        }
                        else
                        {
                            result.DeductItems.Add(item);
                        }
                    }

                    totalEmployeeNetPay += result.NetSalary;
                    totalEPF_Payment += result.OutEPF_EmployeeAmount + result.OutEPF_EmployerAmount;
                    totalSOCSO_Payment += result.OutSOCSO_EmployeeAmount + result.OutSOCSO_EmployerAmount;
                }

                var summary = new PayrollSummaryViewModel
                {
                    Company = company,
                    TotalEmployeeNetPay = Math.Round(totalEmployeeNetPay, 2),
                    TotalEPF_Payment = Math.Round(totalEPF_Payment, 2),
                    TotalSOCSO_Payment = Math.Round(totalSOCSO_Payment, 2),
                    PayslipViewModels = data
                };

                allPayroll.Add(new PayrollHistoryViewModel
                {
                    PayrollSummaryViewModel = summary,
                    PastPayroll = null,
                    PayslipHTML = null,
                    PayslipStaffSequence = null
                });

            }

            return allPayroll;
        }

        public PayrollHistoryViewModel GetLatestPayrollOfStaff(int staffId)
        {
            var company = _context.Companies
                .Include(x => x.Address)
                .Include(x => x.Address.State)
                .FirstOrDefault();
            
            var latestPayslip = _context.Payslips.OrderByDescending(x => x.Date).FirstOrDefault();

            if (latestPayslip == null)
            {
                return null;
            }

            var latestYear = latestPayslip.Date.Year;
            var latestMonth = latestPayslip.Date.Month;

            var payslipsData = _context.Payslips
                .Where(x => x.Date.Year == latestYear && x.Date.Month == latestMonth)
                .Where(x => x.StaffId == staffId)
                .Include(x => x.Staff)
                .Include(x => x.Staff.Address)
                .Include(x => x.Staff.Position)
                .Include(x => x.Staff.EPF_Status)
                .Include(x => x.Staff.SOCSO_Category)
                .Select(x => new PayslipViewModel
                {
                    StaffId = x.StaffId,
                    Staff = x.Staff,
                    BaseSalary = x.BaseSalary,
                    NetSalary = x.NetSalary,
                    VariablePay = x.VariablePay,
                    VariableDeduction = x.VariableDeduction,
                    Date = x.Date,
                    InEPF_EmployeeRate = x.InEPF_EmployeeRate,
                    InEPF_EmployerRate = x.InEPF_EmployerRate,
                    OutEPF_EmployerAmount = x.OutEPF_EmployerAmount,
                    OutEPF_EmployeeAmount = x.OutEPF_EmployeeAmount,
                    OutSOCSO_EmployeeAmount = x.OutSOCSO_EmployeeAmount,
                    OutSOCSO_EmployerAmount = x.OutSOCSO_EmployerAmount,
                    Id = x.Id,
                    Company = company,
                    AddItems = new List<PayslipItem>(),
                    DeductItems = new List<PayslipItem>()
                })
                .ToList();

            double totalEmployeeNetPay = 0.0;
            double totalEPF_Payment = 0.0;
            double totalSOCSO_Payment = 0.0;

            foreach (var result in payslipsData)
            {
                var allItems = _context.PayslipItems.Where(i => i.PayslipId == result.Id).ToList();
                foreach (var item in allItems)
                {
                    var preset = _context.PresetPayslipItems.Find(item.PresetPayslipItemId);
                    item.PresetPayslipItem = preset;
                    if (preset.Direction > 0)
                    {
                        result.AddItems.Add(item);
                    }
                    else
                    {
                        result.DeductItems.Add(item);
                    }
                }

                totalEmployeeNetPay += result.NetSalary;
                totalEPF_Payment += result.OutEPF_EmployeeAmount + result.OutEPF_EmployerAmount;
                totalSOCSO_Payment += result.OutSOCSO_EmployeeAmount + result.OutSOCSO_EmployerAmount;
            }

            var summary = new PayrollSummaryViewModel
            {
                Company = company,
                PayslipViewModels = payslipsData
            };

            var listOfHtml = new List<string>();
            var listOfStaffId = new List<int>();
            for (int i = 0; i < summary.PayslipViewModels.Count; i++)
            {
                listOfHtml.Add("");
                listOfStaffId.Add(0);
            }

            return new PayrollHistoryViewModel
            {
                PayrollSummaryViewModel = summary,
                PastPayroll = GetPastPayrollList(),
                PayslipHTML = listOfHtml,
                PayslipStaffSequence = listOfStaffId
            };
        }


        // methods to be used by other methods

        // calculate EPF
        // employeR first, then employeE
        // R-E
        public Tuple<double, double> CalculateEPF(
            double salaryApplicable,
            Staff staff)
        {
            double employeeRate = staff.EPF_EmployeeRate;
            double employerRate = staff.EPF_EmployerRate;

            double outEmployerAmount, outEmployeeAmount;
            double difference;  // difference between rows in EPF table https://www.kwsp.gov.my/documents/20126/927226/JADUAL_KETIGA_04012019_ENG.pdf/5ab4b811-fa63-081f-e33b-4bea9417343c?t=1559894719914
            double salaryRounded;

            if (salaryApplicable < 0)
            {
                throw new Exception("Less than zero!");
            }
            else if (salaryApplicable <= 10)
            {
                outEmployerAmount = outEmployeeAmount = 0.0;
            }
            else if (salaryApplicable <= 20)
            {
                var rates = _context.EPF_Rates
                    .Where(x => staff.EPF_StatusId == x.EPF_StatusId)
                    .Where(x => salaryApplicable > x.SalaryRangeFrom)
                    .Where(x => staff.DOB <= DateTime.Today.AddYears(-x.AgeFrom))
                    .OrderByDescending(x => x.SalaryRangeFrom)
                    .ThenByDescending(x => x.AgeFrom)
                    .FirstOrDefault();

                if (rates != null)
                {
                    employeeRate += rates.EmployeeRate;
                    employerRate += rates.EmployerRate;
                }
                else
                {
                    employeeRate = employerRate = 0.0;
                }

                outEmployeeAmount = Math.Ceiling(Math.Round(employeeRate * salaryApplicable, 2));
                outEmployerAmount = employerRate >= 1 ?
                    employerRate :
                    Math.Ceiling(Math.Round(employerRate * salaryApplicable, 2));
            }
            else
            {
                if (salaryApplicable <= 20000)
                {
                    difference = salaryApplicable <= 5000 ?
                        20 :
                        100;
                    salaryRounded = Math.Floor((salaryApplicable - 0.01) / difference) * difference + difference;
                }
                else
                {
                    salaryRounded = salaryApplicable;
                }

                var rates = _context.EPF_Rates
                    .Where(x => staff.EPF_StatusId == x.EPF_StatusId)
                    .Where(x => salaryRounded > x.SalaryRangeFrom)
                    .Where(x => staff.DOB <= DateTime.Today.AddYears(-x.AgeFrom))
                    .OrderByDescending(x => x.SalaryRangeFrom)
                    .ThenByDescending(x => x.AgeFrom)
                    .FirstOrDefault();

                if (rates != null)
                {
                    employeeRate += rates.EmployeeRate;
                    employerRate += rates.EmployerRate;
                }
                else
                {
                    employeeRate = employerRate = 0.0;
                }

                outEmployeeAmount = Math.Ceiling(Math.Round(employeeRate * salaryRounded, 2));
                outEmployerAmount = employerRate >= 1 ?
                    employerRate :
                    Math.Ceiling(Math.Round(employerRate * salaryRounded, 2));

            }

            return new Tuple<double, double>(outEmployerAmount, outEmployeeAmount);
        }


        // calculate SOCSO
        // employeR first, then employeE
        // R-E
        public Tuple<double, double> CalculateSOCSO(
            double salaryApplicable,
            SOCSO_Category socso_Category)
        {
            int scheme = socso_Category.Id; // 1 or 2
            Tuple<double, double> tuple;

            if (salaryApplicable < 0)
            {
                throw new Exception("Less than zero");
            }
            else if (salaryApplicable <= 4000)
            {
                 tuple = _context.SOCSO_Rates
                    .Where(x =>
                        x.SOCSO_Category.Id == scheme &&
                        salaryApplicable > x.MinWage &&
                        salaryApplicable <= x.MaxWage)
                    .Select(x => new Tuple<double, double>(x.EmployerAmount, x.EmployeeAmount))
                    .FirstOrDefault();
            }
            else
            {
                tuple = _context.SOCSO_Rates
                    .Where(x =>
                        x.SOCSO_Category.Id == scheme &&
                        x.MaxWage == 4000)
                    .Select(x => new Tuple<double, double>(x.EmployerAmount, x.EmployeeAmount))
                    .FirstOrDefault();
            }

            return tuple;
        }


        // calculate payslip, calling epf and socso after checking whether applicable for each item
        //
        //
        public PayslipViewModel CalculatePayslip(
            Staff staff,
            List<PayslipItem> payslipItems,
            DateTime dateTime)
        {
            Tuple<double, double> tempComputedAmount;
            double outEPF_EmployerAmount, outEPF_EmployeeAmount, outSOCSO_EmployerAmount, outSOCSO_EmployeeAmount;
            double baseSalary = staff.SalaryAmount;
            double epfEmployerRate = staff.EPF_EmployerRate;
            double epfEmployeeRate = staff.EPF_EmployeeRate;
            EPF_Status epf_Status = staff.EPF_Status;
            SOCSO_Category socso_Category = staff.SOCSO_Category;
            double variablePay = 0.0;
            double variableDeduction = 0.0;
            var addItems = new List<PayslipItem>();
            var deductItems = new List<PayslipItem>();
            double salaryApplicableForEPF = baseSalary;
            double salaryApplicableForSOCSO = baseSalary;
            double netSalary;

            foreach (var item in payslipItems)
            {
                if (item.PresetPayslipItem.Direction == 0)
                {
                    throw new Exception("Bad direction");
                }

                if (item.PresetPayslipItem.Direction < 0)
                {
                    deductItems.Add(item);
                    variableDeduction += item.Amount;

                    salaryApplicableForEPF -= item.Amount;
                    salaryApplicableForSOCSO -= item.Amount;
                }
                else
                {
                    addItems.Add(item);
                    variablePay += item.Amount;

                    if (item.PresetPayslipItem.EPF_Applicable)
                    {
                        salaryApplicableForEPF += item.Amount;
                    }
                    
                    if (item.PresetPayslipItem.SOCSO_Applicable)
                    {
                        salaryApplicableForSOCSO += item.Amount;
                    }
                }
            }

            tempComputedAmount = CalculateEPF(salaryApplicableForEPF, staff);
            outEPF_EmployerAmount = tempComputedAmount.Item1;
            outEPF_EmployeeAmount = tempComputedAmount.Item2;

            tempComputedAmount = CalculateSOCSO(salaryApplicableForSOCSO, socso_Category);

            if (tempComputedAmount != null)
            {
                outSOCSO_EmployerAmount = tempComputedAmount.Item1;
                outSOCSO_EmployeeAmount = tempComputedAmount.Item2;
            }
            else
            {
                outSOCSO_EmployerAmount = outSOCSO_EmployeeAmount = 0.0;
            }

            netSalary = baseSalary + variablePay - variableDeduction - outEPF_EmployeeAmount - outSOCSO_EmployeeAmount;
            netSalary = Math.Round(netSalary, 2);

            var existingItems = new List<ItemTuple>();
            foreach (var item in addItems.Concat(deductItems))
            {
                existingItems.Add(new ItemTuple
                {
                    PresetItemId = item.PresetPayslipItemId,
                    ItemAmount = item.Amount
                });
            }

            // return an object/view model, without staff details
            return new PayslipViewModel
            {
                StaffId = staff.Id,
                Staff = staff,
                Date = dateTime,
                BaseSalary = baseSalary,
                NetSalary = netSalary,

                InEPF_EmployerRate = epfEmployerRate,
                InEPF_EmployeeRate = epfEmployeeRate,
                OutEPF_EmployerAmount = outEPF_EmployerAmount,
                OutEPF_EmployeeAmount = outEPF_EmployeeAmount,
                OutSOCSO_EmployerAmount = outSOCSO_EmployerAmount,
                OutSOCSO_EmployeeAmount = outSOCSO_EmployeeAmount,

                // variable
                VariableDeduction = variableDeduction,
                VariablePay = variablePay,
                AddItems = addItems,
                DeductItems = deductItems,
                AvailablePayrollItems = _payslipItemService.GetPresetItems(),
                Company = _context.Companies.FirstOrDefault(),
                ExistingItems = existingItems
            };
        }

        public PayrollSummaryViewModel GetPayslipResults(List<PayslipViewModel> models)
        {
            var results = new List<PayslipViewModel>();

            foreach (var payslip in models)
            {
                payslip.AddItems = new List<PayslipItem>();
                payslip.DeductItems = new List<PayslipItem>();
                if (payslip.ExistingItems == null)
                {
                    payslip.ExistingItems = new List<ItemTuple>();
                }
                else
                {
                    foreach (var itemTuple in payslip.ExistingItems)
                    {
                        var oldItem = _context.PresetPayslipItems.Find(itemTuple.PresetItemId);
                        if (oldItem == null)
                        {
                            // nothing
                        }
                        else if (oldItem.Direction > 0)
                        {
                            payslip.AddItems.Add(new PayslipItem
                            {
                                PresetPayslipItemId = oldItem.Id,
                                Amount = itemTuple.ItemAmount,
                                PresetPayslipItem = oldItem
                            });
                        }
                        else if (oldItem.Direction < 0)
                        {
                            payslip.DeductItems.Add(new PayslipItem
                            {
                                PresetPayslipItemId = oldItem.Id,
                                Amount = itemTuple.ItemAmount,
                                PresetPayslipItem = oldItem
                            });
                        }
                    }
                }

                payslip.Staff = _context.Staffs.Find(payslip.StaffId);
                payslip.Staff.Position = _context.Positions.Find(payslip.Staff.PositionId);
                payslip.Staff.EPF_Status = _context.EPF_Statuses.Find(payslip.Staff.EPF_StatusId);
                payslip.Staff.SOCSO_Category = _context.SOCSO_Categories.Find(payslip.Staff.SOCSO_CategoryId);

                // if not given
                if (payslip.Date.Year < 100)
                {
                    payslip.Date = DateTime.Today;
                }

                var result = CalculatePayslip(payslip.Staff, payslip.AddItems.Concat(payslip.DeductItems).ToList(), payslip.Date);
                results.Add(result);
            }

            double totalEmployeeNetPay = 0.0;
            double totalEPF_Payment = 0.0;
            double totalSOCSO_Payment = 0.0;

            var company = _context.Companies.FirstOrDefault();
            company.Address = _context.Addresses.Find(company.AddressId);
            company.Address.State = _context.States.Find(company.Address.StateId);

            foreach (var result in results)
            {
                totalEmployeeNetPay += result.NetSalary;
                totalEPF_Payment += result.OutEPF_EmployeeAmount + result.OutEPF_EmployerAmount;
                totalSOCSO_Payment += result.OutSOCSO_EmployeeAmount + result.OutSOCSO_EmployerAmount;

                result.Company = company;
            }

            // to add more later
            return new PayrollSummaryViewModel {
                Company = company,
                TotalEmployeeNetPay = totalEmployeeNetPay,
                TotalEPF_Payment = totalEPF_Payment,
                TotalSOCSO_Payment = totalSOCSO_Payment,
                PayslipViewModels = results
            };
        }


        public void CreatePayroll(PayrollSummaryViewModel model)
        {
            foreach (var payslip in model.PayslipViewModels)
            {
                var staff = _context.Staffs.Find(payslip.StaffId);
                var items = new List<PayslipItem>();

                if (payslip.ExistingItems != null)
                {
                    foreach (var itemTuple in payslip.ExistingItems)
                    {
                        var oldItem = _context.PresetPayslipItems.Find(itemTuple.PresetItemId);
                        if (oldItem == null)
                        {
                            // nothing
                        }
                        else if (oldItem.Direction != 0)
                        {
                            items.Add(new PayslipItem
                            {
                                PresetPayslipItemId = oldItem.Id,
                                Amount = itemTuple.ItemAmount
                            });
                        }
                    }
                }

                var newPayslip = new Data.Payslip
                {
                    StaffId = payslip.StaffId,
                    SOCSO_CategoryId = staff.SOCSO_CategoryId,
                    EPF_StatusId = staff.EPF_StatusId,
                    BaseSalary = payslip.BaseSalary,
                    NetSalary = payslip.NetSalary,
                    VariablePay = payslip.VariablePay,
                    VariableDeduction = payslip.VariableDeduction,
                    Date = payslip.Date,
                    InEPF_EmployerRate = payslip.InEPF_EmployerRate,
                    InEPF_EmployeeRate = payslip.InEPF_EmployeeRate,
                    OutEPF_EmployerAmount = payslip.OutEPF_EmployerAmount,
                    OutEPF_EmployeeAmount = payslip.OutEPF_EmployeeAmount,
                    OutSOCSO_EmployerAmount = payslip.OutSOCSO_EmployerAmount,
                    OutSOCSO_EmployeeAmount = payslip.OutSOCSO_EmployeeAmount,
                    PayslipItems = items
                };

                _context.Payslips.Add(newPayslip);
            }

            _context.SaveChanges();
        }


        public List<PayslipViewModel> AcceptNewItem(List<PayslipViewModel> models)
        {
            foreach (var payslip in models)
            {
                payslip.AddItems = new List<PayslipItem>();
                payslip.DeductItems = new List<PayslipItem>();
                payslip.AvailablePayrollItems = _payslipItemService.GetPresetItems();


                payslip.Staff = _context.Staffs
                    .Where(x => x.Id == payslip.StaffId)
                    .Include(x => x.Position)
                    .Include(x => x.EPF_Status)
                    .Include(x => x.SOCSO_Category)
                    .FirstOrDefault();

                payslip.NetSalary = payslip.Staff.SalaryAmount;


                if (payslip.ExistingItems == null)
                {
                    payslip.ExistingItems = new List<ItemTuple>();
                }
                else
                {
                    foreach (var itemTuple in payslip.ExistingItems)
                    {
                        var oldItem = _context.PresetPayslipItems.Find(itemTuple.PresetItemId);
                        if (oldItem == null)
                        {
                            // nothing
                        }
                        else if (oldItem.Direction > 0)
                        {
                            payslip.AddItems.Add(new PayslipItem
                            {
                                PresetPayslipItemId = oldItem.Id,
                                Amount = itemTuple.ItemAmount,
                                PresetPayslipItem = oldItem
                            });
                            payslip.NetSalary += itemTuple.ItemAmount;
                        }
                        else if (oldItem.Direction < 0)
                        {
                            payslip.DeductItems.Add(new PayslipItem
                            {
                                PresetPayslipItemId = oldItem.Id,
                                Amount = itemTuple.ItemAmount,
                                PresetPayslipItem = oldItem
                            });
                            payslip.NetSalary -= itemTuple.ItemAmount;
                        }
                    }
                }

                var item = _context.PresetPayslipItems.Find(payslip.NewItemId);

                if (item == null || payslip.NewItemAmount == 0)
                {
                    // nothing
                }
                else if (item.Direction > 0)
                {
                    payslip.AddItems.Add(new PayslipItem
                    {
                        Amount = payslip.NewItemAmount,
                        PresetPayslipItemId = payslip.NewItemId,
                        PresetPayslipItem = _context.PresetPayslipItems.Find(payslip.NewItemId)
                    });

                    payslip.ExistingItems.Add(new ItemTuple
                    {
                        PresetItemId = payslip.NewItemId,
                        ItemAmount = payslip.NewItemAmount
                    });
                    payslip.NetSalary += payslip.NewItemAmount;
                }
                else if (item.Direction < 0)
                {
                    payslip.DeductItems.Add(new PayslipItem
                    {
                        Amount = payslip.NewItemAmount,
                        PresetPayslipItemId = payslip.NewItemId,
                        PresetPayslipItem = _context.PresetPayslipItems.Find(payslip.NewItemId)
                    });

                    payslip.ExistingItems.Add(new ItemTuple
                    {
                        PresetItemId = payslip.NewItemId,
                        ItemAmount = payslip.NewItemAmount
                    });
                    payslip.NetSalary -= payslip.NewItemAmount;
                }
                else
                {
                    throw new Exception("Bad direction");
                }

                payslip.NewItemAmount = 0.0;
                payslip.NewItemId = 0;
            }

            return models;
        }

        public IEnumerable<SelectListItem> GetPastPayrollList()
        {
            return _context.Payslips
                .GroupBy(O => new { O.Date.Year, O.Date.Month })
                .Select(x => new SelectListItem
                {
                    Text = x.Key.Year.ToString() + "/" + x.Key.Month.ToString().PadLeft(2, '0'),
                    Value = new DateTime(x.Key.Year, x.Key.Month, 1).ToString()
                })
                .ToList();
        }
    }
}
