﻿using EchoPay.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Services
{
    public class LocationService
    {
        private readonly AppDbContext _context;

        public LocationService(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetStatesList()
        {
            return _context.States
                .Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Name
                });
        }

        public IEnumerable<SelectListItem> GetNationsList()
        {
            return _context.Nationalities
                .Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Name
                });
        }

        public int GetIdOfMalaysiaInNations()
        {
            return _context.Nationalities
                .Where(x => x.Name == "Malaysia")
                .Select(x => x.Id)
                .FirstOrDefault();
        }
    }
}
