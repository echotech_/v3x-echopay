﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EchoPay.Data;
using EchoPay.Models.SuperAdmin;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace EchoPay.Services
{
    public class DashboardService
    {
        private readonly AppDbContext _context;
        private readonly CompanyService _companyService;
        private readonly UserManager<User> _userManager;
        private readonly UserService _userService;

        public DashboardService(AppDbContext context,
             CompanyService companyService,
              UserManager<User> userManager,
              UserService userService)
        {
            _context = context;
            _companyService = companyService;
            _userManager = userManager;
            _userService = userService;
        }

        public DashboardViewModel GetDashboardView()
        {
            var dict = new Dictionary<string, int>();
            var staffList = _context.Staffs.Include(x => x.Position).ToList();

            foreach (var position in _context.Positions.ToList())
            {
                dict[position.Name] = 0;
            }

            foreach (var staff in staffList)
            {
                string positionName = staff.Position.Name;
                ++dict[positionName];
            }

            return new DashboardViewModel
            {
                CompanyViewModel = _companyService.GetCompany(),
                StaffNumber = _context.Staffs.Count(),
                AdminNumber = _context.UserClaims
                    .Where(x => x.ClaimType == ClaimTypes.Role && x.ClaimValue == "ADMIN")
                    .Count(),
                PositionNumbersDict = dict
            };
        }

       

    }
}
