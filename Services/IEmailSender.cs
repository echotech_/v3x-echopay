﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Services
{
    public interface IEmailSender
    {
        void Send(string toAddress, string subject, string body, bool sendAsync = true);
        void Send(string toAddress, string subject, string body, MemoryStream memoryStream, bool sendAsync = true);
    }
}
