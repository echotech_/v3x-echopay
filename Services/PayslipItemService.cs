﻿using EchoPay.Data;
using EchoPay.Models.Others;
using EchoPay.Models.Payslip;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Services
{
    public class PayslipItemService
    {
        private readonly AppDbContext _context;
        public PayslipItemService(AppDbContext context)
        {
            _context = context;
        }

        public List<EPFRatesViewModel> GetEPF_RatesList()
        {
            return _context.EPF_Rates
                .OrderBy(x => x.EPF_StatusId)
                .ThenBy(x => x.SalaryRangeFrom)
                .ThenBy(x => x.AgeFrom)
                .Include(x => x.EPF_Status)
                .Select(x => new EPFRatesViewModel
                {
                    Id = x.Id,
                    SalaryRangeFrom = x.SalaryRangeFrom,
                    EPF_StatusId = x.EPF_StatusId,
                    EPF_StatusString = x.EPF_Status.Description,
                    AgeFrom = x.AgeFrom,
                    EmployerRate = x.EmployerRate,
                    EmployeeRate = x.EmployeeRate
                })
                .ToList();
        }

        public IEnumerable<SelectListItem> GetEPF_StatusList()
        {
            return _context.EPF_Statuses
                .Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Description
                });
        }

        public IEnumerable<SelectListItem> GetSOCSO_CategoryList()
        {
            return _context.SOCSO_Categories
                .Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Scheme
                });
        }

        // get presets
        public IEnumerable<SelectListItem> GetPresetItems()
        {
            return _context.PresetPayslipItems.OrderByDescending(x => x.Direction).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = (x.Direction > 0) ?
                        "+ " + x.Name :
                        "- " + x.Name
            });
        }

        public List<PresetPayslipItem> GetPresetPayslipItems()
        {
            return _context.PresetPayslipItems.OrderByDescending(x => x.Direction).ToList();
        }

        public PresetPayslipItemViewModel GetPresetPayslipItemViewModel()
        {
            var directionList = new List<SelectListItem>();
            directionList.Add(new SelectListItem
            {
                Text = "Addition",
                Value = "1"
            });
            directionList.Add(new SelectListItem
            {
                Text = "Deduction",
                Value = "-1"
            });

            var applicableList = new List<SelectListItem>();
            applicableList.Add(new SelectListItem
            {
                Text = "Yes",
                Value = "true"
            });
            applicableList.Add(new SelectListItem
            {
                Text = "No",
                Value = "false"
            });

            return new PresetPayslipItemViewModel
            {
                DirectionList = directionList,
                SOCSO_ApplicableList = applicableList,
                EPF_ApplicableList = applicableList
            };
        }


        public void CreatePresetPayslipItem(PresetPayslipItemViewModel model)
        {
            var preset = new PresetPayslipItem
            {
                Name = model.Name,
                SOCSO_Applicable = model.SOCSO_Applicable,
                EPF_Applicable = model.EPF_Applicable,
                Amount = model.Amount,
                Direction = model.Direction
            };
            _context.PresetPayslipItems.Add(preset);
            _context.SaveChanges();
        }

        public void UpdateEPFRates(List<EPFRatesViewModel> models)
        {
            // mapping
            foreach (var model in models)
            {
                var data = _context.EPF_Rates.Find(model.Id);

                data.SalaryRangeFrom = model.SalaryRangeFrom;
                data.AgeFrom = model.AgeFrom;
                data.EmployerRate = model.EmployerRate;
                data.EmployeeRate = model.EmployeeRate;
            }

            _context.SaveChanges();
        }
    }
}
