﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EchoPay.Data;
using EchoPay.Models;
using EchoPay.Models.Company;
using Microsoft.EntityFrameworkCore.Internal;


namespace EchoPay.Services
{
    public class CompanyService
    {
        private readonly AppDbContext _context;
        private readonly LocationService _locationService;

        public CompanyService(AppDbContext context,
             LocationService locationService)
        {
            _context = context;
            _locationService = locationService;
        }


        public CompanyViewModel GetCompany()
        {
            return _context.Companies
                .Select(x => new CompanyViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Type = x.Type,
                    ContactNo = x.ContactNo,
                    StreetAddress = x.Address.StreetAddress,
                    PostalCode = x.Address.PostalCode,
                    City = x.Address.City,
                    State = x.Address.State.Name,
                    CompanyRegNo = x.CompanyRegNo,
                    E_Number = x.E_Number,
                    C_Number = x.C_Number,
                    EPF_No = x.EPF_No,
                    SOCSO_No = x.SOCSO_No,
                    EndOfMonthPayrollDay = x.EndOfMonthPayrollDay
                })
                .SingleOrDefault();
        }

        public UpdateCompanyCommand GetCompanyForUpdate(int id)
        {
            return _context.Companies
                .Where(x => x.Id == id)
                .Select(x => new UpdateCompanyCommand
                {
                    Id = x.Id,
                    Name = x.Name,
                    Type = x.Type,
                    ContactNo = x.ContactNo,
                    AddressId = x.AddressId,
                    Address = x.Address,
                    StreetAddress = x.Address.StreetAddress,
                    PostalCode = x.Address.PostalCode,
                    City = x.Address.City,
                    StateId = x.Address.StateId,
                    CompanyRegNo = x.CompanyRegNo,
                    E_Number = x.E_Number,
                    C_Number = x.C_Number,
                    EPF_No = x.EPF_No,
                    SOCSO_No = x.SOCSO_No,
                    EndOfMonthPayrollDay = x.EndOfMonthPayrollDay,

                    StateList = _locationService.GetStatesList()
                })
                .SingleOrDefault();
        }

        public void UpdateCompany(UpdateCompanyCommand cmd)
        {
            var company = _context.Companies.Find(cmd.Id);
            if (company == null)
            {
                throw new Exception("Data not found");
            }

            company.Address = _context.Addresses.Find(company.AddressId);

            cmd.UpdateCompany(company);
            _context.SaveChanges();
        }
    }
}
