﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using EchoPay.Models.Company;

namespace EchoPay.Data
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.NoAction;
            }
        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Attendance> Attendances { get; set; }

        public DbSet<Company> Companies { get; set; }
        public DbSet<EPF_Status> EPF_Statuses { get; set; }
        public DbSet<Nationality> Nationalities { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<SOCSO_Category> SOCSO_Categories { get; set; }
        public DbSet<SOCSO_Rate> SOCSO_Rates { get; set; }
        public DbSet<Payslip> Payslips { get; set; }
        public DbSet<PayslipItem> PayslipItems { get; set; }
        public DbSet<PresetPayslipItem> PresetPayslipItems { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<EPF_Rate> EPF_Rates { get; set; }
    }
}