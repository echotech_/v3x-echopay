﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class User : IdentityUser
    {
        // nullable
        public Staff Staff { get; set; } = null!;
    }
}
