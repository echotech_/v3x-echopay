using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ContactNo { get; set; }
        public int AddressId { get; set; }
        public Address Address { get; set; }

        public string CompanyRegNo { get; set; }
        public int E_Number { get; set; }
        public int C_Number { get; set; }
        public int EPF_No { get; set; }
        public int SOCSO_No { get; set; }
        public int EndOfMonthPayrollDay { get; set; }
    }
}
