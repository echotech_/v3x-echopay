﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class PresetPayslipItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public int Direction { get; set; }
        public bool EPF_Applicable { get; set; }
        public bool SOCSO_Applicable { get; set; }
    }
}
