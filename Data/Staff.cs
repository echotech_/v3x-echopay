﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class Staff
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public string ICNo { get; set; }

        public string Name { get; set; }

        public int PositionId { get; set; }
        public Position Position { get; set; }

        public string ContactNo { get; set; }

        public int AddressId { get; set; }
        public Address Address { get; set; }

        public int NationalityId { get; set; }
        public Nationality Nationality { get; set; }

        public DateTime AdmissionDate { get; set; }

        public DateTime DOB { get; set; }

        public double SalaryAmount { get; set; }

        public string EPF_No { get; set; }

        public int EPF_StatusId { get; set; }
        public EPF_Status EPF_Status { get; set;}
        public double EPF_EmployerRate { get; set; }
        public double EPF_EmployeeRate { get; set; }

        public int SOCSO_CategoryId { get; set; }
        public SOCSO_Category SOCSO_Category { get; set; }

        [DefaultValue(false)]
        public bool IsArchived { get; set; }

        public ICollection<Payslip> Payslips { get; set; }
        public ICollection<Attendance> Attendances { get; set; }
    }
}
