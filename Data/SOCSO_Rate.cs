﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class SOCSO_Rate
    {
        public int Id { get; set; }

        public int SOCSO_CategoryId { get; set; }

        public SOCSO_Category SOCSO_Category { get; set; }
        public double MinWage { get; set; }
        public double MaxWage { get; set; }

        public double EmployerAmount { get; set; }

        public double EmployeeAmount { get; set; }
    }
}
