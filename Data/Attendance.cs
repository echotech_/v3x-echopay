using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class Attendance
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public Staff Staff { get; set; }
        public DateTime Date { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime TimeOut { get; set; }
    }
}