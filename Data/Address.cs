using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class Address
    {
        public int Id { get; set; }
        public Staff Staff { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public int StateId { get; set; }
        public State State { get; set; }

        public Company Company { get; set; }
    }
}
