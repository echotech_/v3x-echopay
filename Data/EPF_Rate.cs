﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class EPF_Rate
    {
        public int Id { get; set; }
        public int AgeFrom { get; set; }
        public int EPF_StatusId { get; set; }
        public EPF_Status EPF_Status { get; set; }
        public double SalaryRangeFrom { get; set; }
        public double EmployerRate { get; set; }
        public double EmployeeRate { get; set; }
    }
}
