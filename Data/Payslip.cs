﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class Payslip
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public Staff Staff { get; set; }
        public DateTime Date { get; set; }
        public double BaseSalary { get; set; }
        public double NetSalary { get; set; }
        public double VariablePay { get; set; }
        public double VariableDeduction { get; set; }
        public int EPF_StatusId { get; set; }
        public EPF_Status EPF_Status { get; set; }
        public double InEPF_EmployerRate { get; set; }
        public double InEPF_EmployeeRate { get; set; }
        public double OutEPF_EmployerAmount { get; set; }
        public double OutEPF_EmployeeAmount { get; set; }
        public int SOCSO_CategoryId { get; set; }
        public SOCSO_Category SOCSO_Category { get; set; }
        public double OutSOCSO_EmployerAmount { get; set; }
        public double OutSOCSO_EmployeeAmount { get; set; }

        public ICollection<PayslipItem> PayslipItems { get; set; }
    }
}

