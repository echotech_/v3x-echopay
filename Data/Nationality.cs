﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class Nationality
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Staff> Staffs { get; set; }
    }
}
