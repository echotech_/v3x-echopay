﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public class PayslipItem
    {
        public int Id { get; set; }
        public int PayslipId { get; set; }
        public Payslip Payslip { get; set; }
        public double Amount { get; set; }
        public int PresetPayslipItemId { get; set; }
        public PresetPayslipItem PresetPayslipItem { get; set; }
    }
}
