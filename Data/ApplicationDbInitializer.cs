﻿using EchoPay.Models.Staff;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EchoPay.Data
{
    public static class ApplicationDbInitializer
    {
        public static void SeedData(UserManager<User> userManager, AppDbContext appDbContext)
        {
            // CREATE SUPERADMIN
            if (userManager.FindByNameAsync("superadmin").Result == null)
            {
                // SuperAdmin
                User user = new User
                {
                    UserName = "superadmin",
                    Email = ""
                };

                IdentityResult result = userManager.CreateAsync(user, "corona2020").Result;

                if (result.Succeeded)
                {
                    var newClaim = new Claim(ClaimTypes.Role, "SUPERADMIN");
                    userManager.AddClaimAsync(user, newClaim).Wait();
                }
            }

            // POSITIONS.
            if (appDbContext.Positions.Count() == 0)
            {
                string[] names =
                {
                    "Accountant", "Human Resource Manager", "Warehouse Keeper"
                };
                foreach (var x in names)
                {
                    appDbContext.Positions.Add(new Position
                    {
                        Name = x
                    });
                }
                appDbContext.SaveChanges();
            }

            // STATES
            if (appDbContext.States.Count() == 0)
            {
                string[] names =
                {
                    "JOHOR", "KEDAH", "KELANTAN", "MALACCA", "NEGERI SEMBILAN", "PAHANG", "PENANG", "PERAK", "PERLIS", "SABAH", "SARAWAK", "SELANGOR", "TERENGGANU", "WP KUALA LUMPUR", "WP LABUAN", "WP PUTRAJAYA"
                };
                foreach (var x in names)
                {
                    appDbContext.States.Add(new State
                    {
                        Name = x
                    });
                }
                appDbContext.SaveChanges();
            }

            // STATES
            if (appDbContext.Nationalities.Count() == 0)
            {
                string[] names =
                {
                    "Afghanistan","Aland Islands","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bonaire, Saint Eustatius and Saba","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos Islands","Colombia","Comoros","Cook Islands","Costa Rica","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Democratic Republic of the Congo","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Heard Island and McDonald Islands","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Ivory Coast","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macao","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","North Korea","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestinian Territory","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico","Qatar","Republic of the Congo","Reunion","Romania","Russia","Rwanda","Saint Barthelemy","Saint Helena","Saint Kitts and Nevis","Saint Lucia","Saint Martin","Saint Pierre and Miquelon","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Sint Maarten","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia and the South Sandwich Islands","South Korea","South Sudan","Spain","Sri Lanka","Sudan","Suriname","Svalbard and Jan Mayen","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","U.S. Virgin Islands","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","United States Minor Outlying Islands","Uruguay","Uzbekistan","Vanuatu","Vatican","Venezuela","Vietnam","Wallis and Futuna","Western Sahara","Yemen","Zambia","Zimbabwe"
                };
                foreach (var x in names)
                {
                    appDbContext.Nationalities.Add(new Nationality
                    {
                        Name = x.ToUpper()
                    });
                }
                appDbContext.SaveChanges();
            }

            // Company
            if (appDbContext.Companies.Count() == 0)
            {
                appDbContext.Companies.Add(new Company
                {
                    Name = "V3X Malaysia Sdn. Bhd.",
                    Type = "Corporate",
                    ContactNo = "012-3456789",
                    Address = new Address
                    {
                        StreetAddress = "79A, Jalan Pulai 23, Taman Pulai Utama",
                        City = "Skudai",
                        PostalCode = "81300",
                        StateId = appDbContext.States.Where(x => x.Name == "JOHOR").FirstOrDefault().Id
                    },
                    CompanyRegNo = "ABCD1234",
                    E_Number = 20,
                    C_Number = 50,
                    EPF_No = 123456,
                    SOCSO_No = 654321,
                    EndOfMonthPayrollDay = 28
                });
                appDbContext.SaveChanges();
            }

            // SOCSO category
            if (appDbContext.SOCSO_Categories.Count() == 0)
            {
                SOCSO_Category[] data =
                {
                    new SOCSO_Category
                    {
                        Id = 1,
                        Scheme = "First Category (Employment Injury Scheme and Invalidity Scheme)"
                    },
                    new SOCSO_Category
                    {
                        Id = 2,
                        Scheme = "Second Category (Employment Injury Scheme)"
                    },
                    new SOCSO_Category
                    {
                        Id = 3,
                        Scheme = "N/A"
                    }
                };

                foreach (var x in data)
                {
                    appDbContext.SOCSO_Categories.Add(x);
                }
            }

            // EPF Status
            if (appDbContext.EPF_Statuses.Count() == 0)
            {
                EPF_Status[] data =
                {
                    new EPF_Status
                    {
                        Id = 1,
                        Description = "Malaysian"
                    },
                    new EPF_Status
                    {
                        Id = 2,
                        Description = "Permanent Residents (PR)"
                    },
                    new EPF_Status
                    {
                        Id = 3,
                        Description = "Non-Malaysian who have started to contribute to EPF before 1 August 1998"
                    },
                    new EPF_Status
                    {
                        Id = 4,
                        Description = "Non-Malaysian who have started to contribute to EPF on/after 1 August 1998"
                    },
                    new EPF_Status
                    {
                        Id = 5,
                        Description = "N/A"
                    }
                };

                foreach (var x in data)
                {
                    appDbContext.EPF_Statuses.Add(x);
                }
            }

            // SOCSO data
            if (appDbContext.SOCSO_Rates.Count() == 0)
            {
                // first category
                double[,] data = new double[,]
                {
                    {0,30,0.4,0.1},
                    {30,50,0.7,0.2},
                    {50,70,1.1,0.3},
                    {70,100,1.5,0.4},
                    {100,140,2.1,0.6},
                    {140,200,2.95,0.85},
                    {200,300,4.35,1.25},
                    {300,400,6.15,1.75},
                    {400,500,7.85,2.25},
                    {500,600,9.65,2.75},
                    {600,700,11.35,3.25},
                    {700,800,13.15,3.75},
                    {800,900,14.85,4.25},
                    {900,1000,16.65,4.75},
                    {1000,1100,18.35,5.25},
                    {1100,1200,20.15,5.75},
                    {1200,1300,21.85,6.25},
                    {1300,1400,23.65,6.75},
                    {1400,1500,25.35,7.25},
                    {1500,1600,27.15,7.75},
                    {1600,1700,28.85,8.25},
                    {1700,1800,30.65,8.75},
                    {1800,1900,32.35,9.25},
                    {1900,2000,34.15,9.75},
                    {2000,2100,35.85,10.25},
                    {2100,2200,37.65,10.75},
                    {2200,2300,39.35,11.25},
                    {2300,2400,41.15,11.75},
                    {2400,2500,42.85,12.25},
                    {2500,2600,44.65,12.75},
                    {2600,2700,46.35,13.25},
                    {2700,2800,48.15,13.75},
                    {2800,2900,49.85,14.25},
                    {2900,3000,51.65,14.75},
                    {3000,3100,53.35,15.25},
                    {3100,3200,55.15,15.75},
                    {3200,3300,56.85,16.25},
                    {3300,3400,58.65,16.75},
                    {3400,3500,60.35,17.25},
                    {3500,3600,62.15,17.75},
                    {3600,3700,63.85,18.25},
                    {3700,3800,65.65,18.75},
                    {3800,3900,67.35,19.25},
                    {3900,4000,69.05,19.75}
                };

                for (int i = 0; i < data.GetLength(0); i++)
                {
                    appDbContext.SOCSO_Rates.Add(new SOCSO_Rate
                    {
                        SOCSO_CategoryId = 1,
                        MinWage = data[i, 0],
                        MaxWage = data[i, 1],
                        EmployerAmount = data[i, 2],
                        EmployeeAmount = data[i, 3],
                    });
                }

                //
                // second category
                data = new double[,]
                {
                    {0,30,0.3,0.0},
                    {30,50,0.5,0.0},
                    {50,70,0.8,0.0},
                    {70,100,1.1,0.0},
                    {100,140,1.5,0.0},
                    {140,200,2.1,0.0},
                    {200,300,3.1,0.0},
                    {300,400,4.4,0.0},
                    {400,500,5.6,0.0},
                    {500,600,6.9,0.0},
                    {600,700,8.1,0.0},
                    {700,800,9.4,0.0},
                    {800,900,10.6,0.0},
                    {900,1000,11.9,0.0},
                    {1000,1100,13.1,0.0},
                    {1100,1200,14.4,0.0},
                    {1200,1300,15.6,0.0},
                    {1300,1400,16.9,0.0},
                    {1400,1500,18.1,0.0},
                    {1500,1600,19.4,0.0},
                    {1600,1700,20.6,0.0},
                    {1700,1800,21.9,0.0},
                    {1800,1900,23.1,0.0},
                    {1900,2000,24.4,0.0},
                    {2000,2100,25.6,0.0},
                    {2100,2200,26.9,0.0},
                    {2200,2300,28.1,0.0},
                    {2300,2400,29.4,0.0},
                    {2400,2500,30.6,0.0},
                    {2500,2600,31.9,0.0},
                    {2600,2700,33.1,0.0},
                    {2700,2800,34.4,0.0},
                    {2800,2900,35.6,0.0},
                    {2900,3000,36.9,0.0},
                    {3000,3100,38.1,0.0},
                    {3100,3200,39.4,0.0},
                    {3200,3300,40.6,0.0},
                    {3300,3400,41.9,0.0},
                    {3400,3500,43.1,0.0},
                    {3500,3600,44.4,0.0},
                    {3600,3700,45.6,0.0},
                    {3700,3800,46.9,0.0},
                    {3800,3900,48.1,0.0},
                    {3900,4000,49.4,0.0},
                };

                for (int i = 0; i < data.GetLength(0); i++)
                {
                    appDbContext.SOCSO_Rates.Add(new SOCSO_Rate
                    {
                        SOCSO_CategoryId = 2,
                        MinWage = data[i, 0],
                        MaxWage = data[i, 1],
                        EmployerAmount = data[i, 2],
                        EmployeeAmount = data[i, 3],
                    });
                }

                appDbContext.SaveChanges();
            }

            if (appDbContext.EPF_Rates.Count() == 0)
            {
                EPF_Rate[] data =
                {
                    // below 5000
                    // below 60 yo
                    new EPF_Rate
                    {
                        AgeFrom = 0,
                        EPF_StatusId = 1,
                        SalaryRangeFrom = 0,
                        EmployerRate = 0.13,
                        EmployeeRate = 0.07
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 0,
                        EPF_StatusId = 2,
                        SalaryRangeFrom = 0,
                        EmployerRate = 0.13,
                        EmployeeRate = 0.07
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 0,
                        EPF_StatusId = 3,
                        SalaryRangeFrom = 0,
                        EmployerRate = 0.13,
                        EmployeeRate = 0.07
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 0,
                        EPF_StatusId = 4,
                        SalaryRangeFrom = 0,
                        EmployerRate = 5,       // RM 5 for expats
                        EmployeeRate = 0.07
                    },
                    // below 5000
                    // ABOVE 60 yo
                    new EPF_Rate
                    {
                        AgeFrom = 60,
                        EPF_StatusId = 1,
                        SalaryRangeFrom = 0,
                        EmployerRate = 0.04,
                        EmployeeRate = 0.0
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 60,
                        EPF_StatusId = 2,
                        SalaryRangeFrom = 0,
                        EmployerRate = 0.065,
                        EmployeeRate = 0.055
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 60,
                        EPF_StatusId = 3,
                        SalaryRangeFrom = 0,
                        EmployerRate = 0.065,
                        EmployeeRate = 0.055
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 60,
                        EPF_StatusId = 4,
                        SalaryRangeFrom = 0,
                        EmployerRate = 5,       // RM 5 for expats
                        EmployeeRate = 0.055
                    },
                    // ABOVE 5000
                    // below 60 yo
                    new EPF_Rate
                    {
                        AgeFrom = 0,
                        EPF_StatusId = 1,
                        SalaryRangeFrom = 5000,
                        EmployerRate = 0.12,
                        EmployeeRate = 0.07
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 0,
                        EPF_StatusId = 2,
                        SalaryRangeFrom = 5000,
                        EmployerRate = 0.12,
                        EmployeeRate = 0.07
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 0,
                        EPF_StatusId = 3,
                        SalaryRangeFrom = 5000,
                        EmployerRate = 0.12,
                        EmployeeRate = 0.07
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 0,
                        EPF_StatusId = 4,
                        SalaryRangeFrom = 5000,
                        EmployerRate = 5,       // RM 5 for expats
                        EmployeeRate = 0.07
                    },
                    // ABOVE 5000
                    // ABOVE 60 yo
                    new EPF_Rate
                    {
                        AgeFrom = 60,
                        EPF_StatusId = 1,
                        SalaryRangeFrom = 5000,
                        EmployerRate = 0.04,
                        EmployeeRate = 0.0
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 60,
                        EPF_StatusId = 2,
                        SalaryRangeFrom = 5000,
                        EmployerRate = 0.06,
                        EmployeeRate = 0.055
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 60,
                        EPF_StatusId = 3,
                        SalaryRangeFrom = 5000,
                        EmployerRate = 0.06,
                        EmployeeRate = 0.055
                    },
                    new EPF_Rate
                    {
                        AgeFrom = 60,
                        EPF_StatusId = 4,
                        SalaryRangeFrom = 5000,
                        EmployerRate = 5,       // RM 5 for expats
                        EmployeeRate = 0.055
                    }
                };

                appDbContext.EPF_Rates.AddRange(data);
                appDbContext.SaveChanges();
            }

            // check again
            if (appDbContext.PresetPayslipItems.Count() == 0)
            {
                PresetPayslipItem[] data =
                {
                    new PresetPayslipItem
                    {
                        Name = "Bonus",
                        Amount = 0.0,
                        Direction = 1,
                        EPF_Applicable = true,
                        SOCSO_Applicable = true
                    },
                    new PresetPayslipItem
                    {
                        Name = "Advanced Pay Addition",
                        Amount = 0.0,
                        Direction = 1,
                        EPF_Applicable = true,
                        SOCSO_Applicable = true
                    },
                    new PresetPayslipItem
                    {
                        Name = "Advanced Pay Deduction",
                        Amount = 0.0,
                        Direction = -1,
                        EPF_Applicable = true,
                        SOCSO_Applicable = true
                    }
                };

                appDbContext.PresetPayslipItems.AddRange(data);
                appDbContext.SaveChanges();
            }

        }
    }
}
