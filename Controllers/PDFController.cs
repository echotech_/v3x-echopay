﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SelectPdf;
using System.IO.Compression;
using System.Net;
using EchoPay.Data;
using EchoPay.Services;

namespace EchoPay.Controllers
{
    public class PDFController : Controller
    {
        public string[] PayslipOuterHTML =
            {
                "<!DOCTYPE html> <html lang=\"en\"> <head> <meta charset=\"utf-8\" /><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" /><link rel=\"stylesheet\" type=\"text/css\" href=\"https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons\" /><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css\"><link href=\"/dashboard-assets/css/material-dashboard.css?v=2.1.2\" rel=\"stylesheet\" /> <title> EchoPay </title> <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' /> </head> <body style=\"background-color: #ffffff; scrollbar-width: none;-ms-overflow-style: none;overflow:hidden;\">",
                "</body></html>"
            };
        private readonly IEmailSender _emailSender;
        private readonly StaffService _staffService;

        public PDFController(StaffService staffService, IEmailSender emailSender)
        {
            _emailSender = emailSender;
            _staffService = staffService;
        }

        public IActionResult GeneratePDF(string HTMLcontent)
        {
            PdfDocument doc = GetPayslipFromHTML(HTMLcontent);

            // create memory stream to save PDF
            MemoryStream pdfStream = new MemoryStream();

            // save pdf document into a MemoryStream
            doc.Save(pdfStream);
            // reset stream position
            pdfStream.Position = 0;
            // close pdf document
            doc.Close();

            return new FileStreamResult(pdfStream, "application/pdf");
        }

        public IActionResult DownloadList(string[] PayslipHTML, int[] PayslipStaffSequence)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    for (int i = 0; i < PayslipHTML.Length; i++)
                    {
                        var document = GetPayslipFromHTML(PayslipHTML[i]);
                        string name = _staffService.GetStaffById(PayslipStaffSequence[i]).Name;
                        AddToArchive(ziparchive, DateTime.Now.ToString("yyyy-MM-dd") + "_" + name + ".pdf", document.Save());
                    }
                }
                return File(memoryStream.ToArray(), "application/zip", "Attachments.zip");
            }
        }

        public IActionResult SendEmailToAll(string [] PayslipHTML, int[] PayslipStaffSequence)
        {
            if (PayslipHTML.Length != PayslipStaffSequence.Length)
            {
                throw new Exception("Why????");
            }

            for (int i = 0; i < PayslipHTML.Length; i++)
            {
                Staff toStaff = _staffService.GetStaffById(PayslipStaffSequence[i]);
                PdfDocument doc = GetPayslipFromHTML(PayslipHTML[i]);

                // create memory stream to save PDF
                MemoryStream pdfStream = new MemoryStream();

                // save pdf document into a MemoryStream
                doc.Save(pdfStream);
                // reset stream position
                pdfStream.Position = 0;

                _emailSender.Send(
                    toStaff.User.Email,
                    "Payslip",
                    "Download the payslip in attachment.",
                    pdfStream,
                    true);

                // close pdf document
                doc.Close();
            }

            return View();
        }

        public IActionResult SendEmailToAllAndDownload(string[] PayslipHTML, int[] PayslipStaffSequence)
        {
            if (PayslipHTML.Length != PayslipStaffSequence.Length)
            {
                throw new Exception("Why????");
            }
            List<byte[]> documents = new List<byte[]>();
            List<string> staffNames = new List<string>();

            for (int i = 0; i < PayslipHTML.Length; i++)
            {
                Staff toStaff = _staffService.GetStaffById(PayslipStaffSequence[i]);
                staffNames.Add(toStaff.Name);

                PdfDocument doc = GetPayslipFromHTML(PayslipHTML[i]);
                

                // create memory stream to save PDF
                MemoryStream pdfStream = new MemoryStream();

                // save pdf document into a MemoryStream
                doc.Save(pdfStream);
                // reset stream position
                pdfStream.Position = 0;
                documents.Add(pdfStream.ToArray());

                _emailSender.Send(
                    toStaff.User.Email,
                    "Payslip",
                    "Download the payslip in attachment.",
                    pdfStream,
                    true);

                // close pdf document
                doc.Close();
            }

            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    for (int i = 0; i < documents.Count; i++)
                    {
                        AddToArchive(ziparchive, DateTime.Now.ToString("yyyy-MM-dd")+ "_" + staffNames[i] + ".pdf", documents[i]);
                    }
                }
                return File(memoryStream.ToArray(), "application/zip", "Attachments.zip");
            }
        }



        private PdfDocument GetPayslipFromHTML(string html)
        {
            string root = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            string newHtml = PayslipOuterHTML[0] + html + PayslipOuterHTML[1];

            // set converter options
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 20;

            converter.Options.WebPageWidth = 1024;
            converter.Options.WebPageHeight = 0;
            converter.Options.WebPageFixedSize = false;

            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.AutoFitHeight = HtmlToPdfPageFitMode.NoAdjustment;

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(newHtml, root);
            return doc;
        }

        private void AddToArchive(ZipArchive ziparchive, string fileName, byte[] attach)
        {
            var zipEntry = ziparchive.CreateEntry(fileName, CompressionLevel.Optimal);
            using (var zipStream = zipEntry.Open())
            using (var streamIn = new MemoryStream(attach))
            {
                streamIn.CopyTo(zipStream);
            }
        }
    }
}
