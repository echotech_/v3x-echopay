using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EchoPay.Data;
using EchoPay.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Math.EC.Rfc7748;

namespace EchoPay.Controllers
{
    [Authorize]
    public class StaffController : Controller
    {
        private readonly PayslipService _payslipService;
        private readonly AppDbContext _context;

        public StaffController(PayslipService payslipService, AppDbContext context)
        {
            _payslipService = payslipService;
            _context = context;
        }

        public IActionResult Index()
        {
            Data.Staff userStaff = _context.Staffs
                .Where(x => x.UserId == User.FindFirstValue(ClaimTypes.NameIdentifier))
                .FirstOrDefault();

            if (userStaff == null)
            {
                return RedirectToAction("Index", "Admin");
            }

            return RedirectToAction("Payslip");
        }

        public IActionResult Payslip()
        {

            Data.Staff userStaff = _context.Staffs
                .Where(x => x.UserId == User.FindFirstValue(ClaimTypes.NameIdentifier))
                .FirstOrDefault();

            if (userStaff == null)
            {
                return RedirectToAction("Index", "Admin");
            }

            int staffId = userStaff.Id;
            var model = _payslipService.GetSingleStaffReport(staffId);
            return View(model);
        }

        public IActionResult Report()
        {
            Data.Staff userStaff = _context.Staffs
                .Where(x => x.UserId == User.FindFirstValue(ClaimTypes.NameIdentifier))
                .FirstOrDefault();

            if (userStaff == null)
            {
                return RedirectToAction("Index", "Admin");
            }

            int staffId = userStaff.Id;
            var model = _payslipService.GetSingleStaffReport(staffId);
            return View(model);
        }
    }
}
