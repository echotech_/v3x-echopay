using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EchoPay.Data;
using EchoPay.Models.User;
using EchoPay.Models.Company;
using EchoPay.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using EchoPay.Models.SuperAdmin;
using EchoPay.Models.Staff;
using System.Security.Claims;

namespace EchoPay.Controllers
{
    [Authorize("SuperAdmin")]
    public class SuperAdminController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly UserService _userService;
        private readonly CompanyService _companyService;
        private readonly SignInManager<User> _signInManager;
        private readonly DashboardService _dashboardService;
        private readonly PayslipService _payslipService;
        private readonly StaffService _staffService;
        private readonly AppDbContext _context;

        public SuperAdminController(
            UserManager<User> userManager,
            UserService userService,
            CompanyService companyService,
            SignInManager<User> signInManager,
            PayslipService payslipService,
            DashboardService dashboardService,
            StaffService staffService,
            AppDbContext context
            )
        {
            _userManager = userManager;
            _userService = userService;
            _companyService = companyService;
            _signInManager = signInManager;
            _payslipService = payslipService;
            _dashboardService = dashboardService;
            _staffService = staffService;
            _context = context;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Dashboard");
        }

        public IActionResult Dashboard()
        {
            var models = _dashboardService.GetDashboardView();                
            return View(models);
        }

        public IActionResult Company(string id = null)
        {
            int parsedId;
            Int32.TryParse(id, out parsedId);
            var models = _companyService.GetCompanyForUpdate(parsedId);
            return View("UpdateCompany", models);
        }

        public IActionResult UpdateCompany(UpdateCompanyCommand cmd)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _companyService.UpdateCompany(cmd);
                    return RedirectToAction("Dashboard");
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError(
                    string.Empty,
                    "An error occurred while saving the item"
                );
            }
            return View("UpdateCompany", cmd);
        }

        [HttpGet]
        public async Task<IActionResult> Admin()
        {
            var models = await _userService.GetAllAdminsPageViewAsync();

            return View(models);
        }

        [HttpPost]
        public async Task<IActionResult> Admin(CreateAdminCommand cmd)
        {
            if (cmd.NewPassword != cmd.ConfirmPassword)
            {
                var _models = await _userService.GetAllAdminsPageViewAsync();
                _models.CreateAdminCommand = cmd;
                return View(_models);
            }

            if (await _userService.CreateAdmin(cmd))
            {
                return RedirectToAction("Admin");
            }

            var models = await _userService.GetAllAdminsPageViewAsync();
            models.CreateAdminCommand = cmd;
            return View(models);
        }

        [HttpPost]
        public IActionResult TerminateAdmin(string id)
        {
            _userService.TerminateAdminAsync(id).Wait();

            return RedirectToAction("Admin");
        }

        [HttpPost]
        public IActionResult SetAsAdmin(string id)
        {
            _userService.SetAsAdmin(id).Wait();
            return RedirectToAction("Admin");
        }

        public IActionResult Report()
        {
            var models = _payslipService.GetAllPayroll();
            return View(models);
        }
        
        public IActionResult Testing()
        {
            return View();
        }

        public async Task<IActionResult> AddRandomStaff(int number)
        {
            Random rand = new Random();

            if (number < 1)
            {
                return RedirectToAction("Testing");
            }

            List<int> socsoIdList = _context.SOCSO_Categories.Select(x => x.Id).ToList();
            List<int> stateIdList = _context.States.Select(x => x.Id).ToList();
            List<int> epfIdList = _context.EPF_Statuses.Select(x => x.Id).ToList();
            List<int> nationIdList = _context.Nationalities.Select(x => x.Id).ToList();
            List<int> positionIdList = _context.Positions.Select(x => x.Id).ToList();


            int i = 0;
            while (i++ < number)
            {
                string randomString = RandomString(10);

                // create user by email
                var user = new User
                {
                    UserName = randomString + "@testabc1234.tttttt",
                    Email = randomString + "@testabc1234.tttttt"
                };

                CreateStaffCommand cmd = new CreateStaffCommand
                {
                    SalaryAmount = Math.Round(rand.NextDouble() * 10000 + 1000, 2),
                    SOCSO_CategoryId = socsoIdList.ElementAt(rand.Next(0, socsoIdList.Count)),
                    StateId = stateIdList.ElementAt(rand.Next(0, stateIdList.Count)),
                    StreetAddress = "123",
                    EPF_StatusId = epfIdList.ElementAt(rand.Next(0, epfIdList.Count)),
                    ContactNo = "0191010101",
                    DOB = DateTime.Today.AddYears(-20 - rand.Next(30)),
                    AdmissionDate = DateTime.Today.AddYears(-20 - rand.Next(30)),
                    City = "city",
                    PositionId = positionIdList.ElementAt(rand.Next(0, positionIdList.Count)),
                    EPF_EmployeeRate = 0.0,
                    EPF_EmployerRate = 0.0,
                    NationalityId = nationIdList.ElementAt(rand.Next(0, nationIdList.Count)),
                    EPF_No = "123414314",
                    PostalCode = rand.Next(1000, 99999).ToString(),
                    ICNo = "770707070700",
                    Name = randomString
                };


                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    var newClaim = new Claim(ClaimTypes.Role, "STAFF");
                    _userManager.AddClaimAsync(user, newClaim).Wait();
                    cmd.UserId = user.Id;
                    _staffService.CreateStaff(cmd);
                }

            }

            return RedirectToAction("Testing");
        }


        public IActionResult RemoveAllStaff()
        {
            string superAdminId = _context.Users.Where(x => x.UserName == "superadmin").FirstOrDefault().Id;

            _context.Attendances.RemoveRange(_context.Attendances);
            _context.PayslipItems.RemoveRange(_context.PayslipItems);
            _context.Payslips.RemoveRange(_context.Payslips);
            _context.Staffs.RemoveRange(_context.Staffs);
            _context.UserClaims.RemoveRange(_context.UserClaims.Where(x => x.UserId != superAdminId));
            _context.UserTokens.RemoveRange(_context.UserTokens.Where(x => x.UserId != superAdminId));
            _context.Users.RemoveRange(_context.Users.Where(x => x.Id != superAdminId));

            _context.SaveChanges();

            return RedirectToAction("Testing");
        }

        public IActionResult AddPastPayroll(int number)
        {
            if (number < 1 || _context.Staffs.Count() == 0)
            {
                return RedirectToAction("Testing");
            }

            int i = 0;
            while (i < number)
            {
                var newDate = DateTime.Today.AddMonths(-1 - i);
                var models = _payslipService.SeedPayrollComplexModel(newDate);
                var results = _payslipService.GetPayslipResults(models);
                _payslipService.CreatePayroll(results);

                i++;
            }

            return RedirectToAction("Testing");
        }

        public IActionResult ClearPayrollHistory()
        {
            _context.PayslipItems.RemoveRange(_context.PayslipItems);
            _context.Payslips.RemoveRange(_context.Payslips);
            _context.SaveChanges();

            return RedirectToAction("Testing");
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[new Random().Next(s.Length)]).ToArray());
        }
    }
}
