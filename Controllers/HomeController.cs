﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EchoPay.Models;
using Microsoft.AspNetCore.Identity;
using EchoPay.Data;
using System.Security.Claims;
using EchoPay.Services;
using Microsoft.AspNetCore.Authorization;
using EchoPay.Models.User;

namespace EchoPay.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IEmailSender _emailSender;
        // might be refactored later into separate service (UserService)

        public HomeController(
            ILogger<HomeController> logger,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IEmailSender emailSender)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }

        // Index is Login
        [HttpGet]
        public IActionResult Index()
        {
            // If already logged in
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", User.FindFirst(ClaimTypes.Role)?.Value.ToLower(), new { area = "" });
            }

            // Let user login
            return View();
        }

        // Index is Login, POST means action
        [HttpPost]
        public async Task<IActionResult> Index(string username, string password)
        {
            var user = _userManager.Users.FirstOrDefault(
                u => u.UserName == username || u.Email == username);
            if (user != null)
            {
                var signInResult = await _signInManager.PasswordSignInAsync(user, password, false, false);
                if (signInResult.Succeeded)
                {
                    // do nothing, proceed to below, return to index to redirect
                }
                else
                {
                    ViewData["msg"] = "Not found!";
                    return View();
                }
            }
            else
            {
                ViewData["msg"] = "Not found!";
                return View();
            }

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult ForgetPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ForgetPassword(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                ViewData["Message"] = "This email is not registered with us.";
                // add msg in view later
                return View();
            }
            // user found
            // send email to the registered email
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var link = Url.Action(nameof(HomeController.ResetPassword), "Home", new { userId = user.Id, code }, Request.Scheme, Request.Host.ToString());
            _emailSender.Send(user.Email, "Reset password", $"<a href=\"{link}\">Reset password for EchoPay System</a>", true);

            return RedirectToAction("EmailSent");
        }

        public async Task<IActionResult> ResetPassword(string userId, string code)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null) return BadRequest();

            var result = await _userManager.VerifyUserTokenAsync(
                                    user,
                                    _userManager.Options.Tokens.PasswordResetTokenProvider,
                                    UserManager<User>.ResetPasswordTokenPurpose,
                                    code);

            if (result)
            {
                ViewData["userId"] = userId;
                ViewData["code"] = code;
                ViewData["action"] = "ResetPassword";
                return View("ResetPassword");
            }

            return BadRequest();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(SetPasswordViewModel model, string userId, string code)
        {
            if (userId == null || code == null)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                return BadRequest();

            var result = await _userManager.ResetPasswordAsync(user, code, model.NewPassword);
            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                return RedirectToAction("Index");
            }

            ViewData["userId"] = userId;
            ViewData["code"] = code;
            ViewData["action"] = "ResetPassword";

            return View(model);
        }

        // verify email and let user set password
        public async Task<IActionResult> VerifyEmail(string userId, string code)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null) return BadRequest();

            var result = await _userManager.ConfirmEmailAsync(user, code);

            if (result.Succeeded)
            {
                ViewData["userId"] = userId;
                ViewData["code"] = code;
                ViewData["action"] = "VerifyEmail";
                return View("ResetPassword");
            }
            return BadRequest();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> VerifyEmail(SetPasswordViewModel model, string userId, string code)
        {
            if (userId == null || code == null)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                return BadRequest();

            var result = await _userManager.AddPasswordAsync(user, model.NewPassword);
            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                return RedirectToAction("Index");
            }

            ViewData["userId"] = userId;
            ViewData["code"] = code;
            ViewData["action"] = "VerifyEmail";
            return View("ResetPassword" ,model);
        }


        public IActionResult EmailSent() => View();

        // End of email section


        public IActionResult Privacy()
        {
            return View();
        }

        [Route("Home/Error/{statusCode}")]
        public IActionResult Error(int statusCode)
        {
            ViewData["code"] = statusCode;
            if (statusCode == 404)
                ViewData["msg"] = "Page not found.";

            return View();
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);
                if (user == null)
                {
                    return RedirectToAction("Login");
                }

                // ChangePasswordAsync changes the user password
                var result = await _userManager.ChangePasswordAsync(user,
                    model.CurrentPassword, model.NewPassword);

                // The new password did not meet the complexity rules or
                // the current password is incorrect. Add these errors to
                // the ModelState and rerender ChangePassword view
                if (!result.Succeeded)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return View();
                }

                // Upon successfully changing the password refresh sign-in cookie
                await _signInManager.RefreshSignInAsync(user);
                return View("_ChangePasswordConfirmationView");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}
