﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EchoPay.Data;
using EchoPay.Models;
using EchoPay.Models.Others;
using EchoPay.Models.Payslip;
using EchoPay.Models.Staff;
using EchoPay.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Utilities;

namespace EchoPay.Controllers
{
    [Authorize("Admin")]
    public class AdminController : Controller
    {
        private readonly StaffService _staffService;
        private readonly IEmailSender _emailSender;
        private readonly UserManager<User> _userManager;
        private readonly PayslipService _payslipService;
        private readonly PayslipItemService _payslipItemService;

        public AdminController(
            StaffService staffService,
            UserManager<User> userManager,
            IEmailSender emailSender,
            PayslipService payslipService,
            PayslipItemService payslipItemService)
        {
            _staffService = staffService;
            _emailSender = emailSender;
            _userManager = userManager;
            _payslipService = payslipService;
            _payslipItemService = payslipItemService;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Attendance");
        }

        [HttpGet]
        public IActionResult Attendance(string id = null)
        {
            int parsedId;
            if (id == null)
            {
                var models = _staffService.GetAdminAttendancePageView();
                return View(models);
            }
            else if (Int32.TryParse(id, out parsedId))
            {
                var models = _staffService.GetStaffForAttendance(parsedId);
                return View("UpdateAttendance", models);
            }

            return View("Error");
        }

        [HttpPost]
        public IActionResult UpdateAttendanceDate(UpdateStaffAttendanceCommand cmd)
        {
            cmd = _staffService.GetStaffForAttendance(cmd.StaffId, cmd.StartDate, cmd.EndDate);
            return View("UpdateAttendance", cmd);
        }

        [HttpPost]
        public IActionResult UpdateAttendance(List<CreateAttendanceCommand> cmd)
        {

            foreach (var att in cmd)
            {
                // add new attendance
                if (att.AttendedFlag > 0 && att.Id <= 0)
                {
                    _staffService.CreateAttendance(att);
                }
                // update present to absent
                else if (att.AttendedFlag == 0 && att.Id > 0)
                {
                    _staffService.RemoveAttendance(att);
                }
                // else do nothing
            }

            return RedirectToAction("Attendance");
        }

        public IActionResult Payslip()
        {
            return View();
        }

        public IActionResult Staff(string id = null)
        {
            if (id == null)
            {
                var models = _staffService.GetAdminStaffPageView();
                return View(models);
            }
            else if (Int32.TryParse(id, out int parsedId))
            {
                var models = _staffService.GetStaffForUpdate(parsedId);
                return View("UpdateStaff", models);
            }

            return View("Error");
        }

        [HttpPost]
        public IActionResult UpdateStaff(UpdateStaffCommand cmd)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _staffService.UpdateStaff(cmd);
                    return RedirectToAction(nameof(Staff));
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError(
                    string.Empty,
                    "An error occurred while saving the item"
                );
            }
            return View("UpdateStaff", cmd);
        }

        public IActionResult Report(string id = null)
        {
            if (id == null)
            {
                var staffs = _staffService.GetStaffs();
                return View(staffs);
            }

            var report = _payslipService.GetSingleStaffReport(int.Parse(id));
            return View("ReportSingleStaff", report);
        }

        public IActionResult Settings()
        {
            var model = new SettingsViewModel
            {
                PositionsList = _staffService.GetPositionsList().ToList(),
                PresetItemsList = _payslipItemService.GetPresetPayslipItems(),
                PresetPayslipItemViewModel = _payslipItemService.GetPresetPayslipItemViewModel(),
                CreatePositionCommand = new CreatePositionCommand
                {
                    Name = ""
                },
                EPFRatesViewModelList = _payslipItemService.GetEPF_RatesList()
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult UpdateEPFRates(List<EPFRatesViewModel> models)
        {
            _payslipItemService.UpdateEPFRates(models);

            return RedirectToAction("Settings");
        } 

        public IActionResult CreatePosition(CreatePositionCommand cmd)
        {
            _staffService.CreatePosition(cmd);

            return RedirectToAction("Settings");
        }

        public IActionResult CreatePayslipItem(PresetPayslipItemViewModel model)
        {
            _payslipItemService.CreatePresetPayslipItem(model);

            return RedirectToAction("Settings");
        }


        public async Task<IActionResult> CreateStaff(CreateStaffCommand cmd)
        {
            var command = cmd;
            bool successful = false;

            // bind to this admin account (user already exists)
            if (cmd.CreateStaffOption == 2)
            {
                successful = await _staffService.CreateStaffAndBindToAdminAsync(cmd);
            }
            else if (cmd.CreateStaffOption == 1)
            {
                // create user by email
                var user = new User
                {
                    UserName = cmd.Email,
                    Email = cmd.Email
                };

                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    var newClaim = new Claim(ClaimTypes.Role, "STAFF");
                    _userManager.AddClaimAsync(user, newClaim).Wait();
                    cmd.UserId = user.Id;
                    _staffService.CreateStaff(cmd);

                    // send email to the registered email
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var link = Url.Action(nameof(HomeController.VerifyEmail), "Home", new { userId = user.Id, code }, Request.Scheme, Request.Host.ToString());
                    _emailSender.Send(user.Email, "Verify email", $"<a href=\"{link}\">Verify Email for EchoPay System</a>", true);

                    successful = true;
                }
            }
            
            // add error validation later

            // redirect to email sent page
            return RedirectToAction("Staff");
        }
        
        public IActionResult NewPayroll()
        {
            // if this month already submitted before, return null
            var model = _payslipService.CheckThisMonthSubmitted() ?
                null :
                _payslipService.GetNewPayrollComplexModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult NewPayroll(List<PayslipViewModel> models)
        {
            ModelState.Clear();
            var newModels = _payslipService.AcceptNewItem(models);
            return View(newModels);
        }

        [HttpPost]
        public IActionResult ReturnNewPayroll(PayrollSummaryViewModel model)
        {
            //ModelState.Clear();
            //var models = model.PayslipViewModels;
            //models = _payslipService.AcceptNewItem(models);
            //return View("NewPayroll", models);
            return RedirectToAction("NewPayroll");
        }

        public IActionResult PayrollSummary(List<PayslipViewModel> models)
        {
            var results = _payslipService.GetPayslipResults(models);
            return View(results);
        }
        
        public IActionResult CompletePayroll(PayrollSummaryViewModel model)
        {
            // do something for DB
            _payslipService.CreatePayroll(model);
            return RedirectToAction("PayrollHistory");
        }

        [HttpGet]
        public IActionResult PayrollHistory()
        {
            var model = _payslipService.GetLatestPayroll();
            return View(model);
        }

        [HttpPost]
        public IActionResult PayrollHistory(PayrollHistoryViewModel model)
        {
            if (model == null)
            {
                model = _payslipService.GetLatestPayroll();
            }
            else
            {
                model = _payslipService.GetPayrollByDateString(model.SelectedPayroll);
            }
            return View(model);
        }
    }
}