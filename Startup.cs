using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EchoPay.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Security.Claims;
using EchoPay.Services;
using Microsoft.AspNetCore.Http;

namespace EchoPay
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connString = Configuration["Data:Test:ConnectionString"];
            services.AddDbContext<AppDbContext>(
                options => options.UseSqlServer(connString));

            
            services.AddIdentity<User, IdentityRole>(config =>
            {
                config.Password.RequiredLength = 4;
                config.Password.RequireDigit = false;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireUppercase = false;
                config.SignIn.RequireConfirmedEmail = false;
            })
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(config =>
            {
                config.Cookie.Name = "Identity.Cookie";
                config.LoginPath = "/Home";
                config.AccessDeniedPath = "/Home/AccessDenied";
            });

            services.Configure<DataProtectionTokenProviderOptions>(o => o.TokenLifespan = TimeSpan.FromDays(3650));


            services.AddControllersWithViews();

            services.AddAuthorization(options => {
                options.AddPolicy("SuperAdmin",
                    policyBuilder => policyBuilder
                        .RequireClaim(ClaimTypes.Role, "SUPERADMIN"));
            });

            services.AddAuthorization(options => {
                options.AddPolicy("Admin",
                    policyBuilder => policyBuilder
                        .RequireClaim(ClaimTypes.Role, "ADMIN"));
            });

            services.AddHttpContextAccessor();

            services.AddScoped<IEmailSender, EmailSender>();
            services.AddScoped<StaffService>();
            services.AddScoped<UserService>();
            services.AddScoped<PayslipItemService>();
            services.AddScoped<PayrollHistoryService>();
            services.AddScoped<PayslipService>();
            services.AddScoped<LocationService>();
            services.AddScoped<CompanyService>();
            services.AddScoped<DashboardService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<User> userManager, AppDbContext appDbContext)
        {
            app.UseStatusCodePagesWithRedirects("/Home/Error/{0}");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
            
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });



            ApplicationDbInitializer.SeedData(userManager, appDbContext);
        }
    }
}
