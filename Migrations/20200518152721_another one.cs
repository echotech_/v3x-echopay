﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EchoPay.Migrations
{
    public partial class anotherone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payslip_EPF_Statuses_EPF_StatusId",
                table: "Payslip");

            migrationBuilder.DropForeignKey(
                name: "FK_Payslip_SOCSO_Categories_SOCSO_CategoryId",
                table: "Payslip");

            migrationBuilder.DropForeignKey(
                name: "FK_Payslip_Staffs_StaffId",
                table: "Payslip");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Payslip",
                table: "Payslip");

            migrationBuilder.RenameTable(
                name: "Payslip",
                newName: "Payslips");

            migrationBuilder.RenameIndex(
                name: "IX_Payslip_StaffId",
                table: "Payslips",
                newName: "IX_Payslips_StaffId");

            migrationBuilder.RenameIndex(
                name: "IX_Payslip_SOCSO_CategoryId",
                table: "Payslips",
                newName: "IX_Payslips_SOCSO_CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_Payslip_EPF_StatusId",
                table: "Payslips",
                newName: "IX_Payslips_EPF_StatusId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Payslips",
                table: "Payslips",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "PayslipItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PayslipId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    Direction = table.Column<int>(nullable: false),
                    EPF_Applicable = table.Column<bool>(nullable: false),
                    SOCSO_Applicable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayslipItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PayslipItems_Payslips_PayslipId",
                        column: x => x.PayslipId,
                        principalTable: "Payslips",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_PayslipItems_PayslipId",
                table: "PayslipItems",
                column: "PayslipId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payslips_EPF_Statuses_EPF_StatusId",
                table: "Payslips",
                column: "EPF_StatusId",
                principalTable: "EPF_Statuses",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Payslips_SOCSO_Categories_SOCSO_CategoryId",
                table: "Payslips",
                column: "SOCSO_CategoryId",
                principalTable: "SOCSO_Categories",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Payslips_Staffs_StaffId",
                table: "Payslips",
                column: "StaffId",
                principalTable: "Staffs",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payslips_EPF_Statuses_EPF_StatusId",
                table: "Payslips");

            migrationBuilder.DropForeignKey(
                name: "FK_Payslips_SOCSO_Categories_SOCSO_CategoryId",
                table: "Payslips");

            migrationBuilder.DropForeignKey(
                name: "FK_Payslips_Staffs_StaffId",
                table: "Payslips");

            migrationBuilder.DropTable(
                name: "PayslipItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Payslips",
                table: "Payslips");

            migrationBuilder.RenameTable(
                name: "Payslips",
                newName: "Payslip");

            migrationBuilder.RenameIndex(
                name: "IX_Payslips_StaffId",
                table: "Payslip",
                newName: "IX_Payslip_StaffId");

            migrationBuilder.RenameIndex(
                name: "IX_Payslips_SOCSO_CategoryId",
                table: "Payslip",
                newName: "IX_Payslip_SOCSO_CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_Payslips_EPF_StatusId",
                table: "Payslip",
                newName: "IX_Payslip_EPF_StatusId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Payslip",
                table: "Payslip",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Payslip_EPF_Statuses_EPF_StatusId",
                table: "Payslip",
                column: "EPF_StatusId",
                principalTable: "EPF_Statuses",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Payslip_SOCSO_Categories_SOCSO_CategoryId",
                table: "Payslip",
                column: "SOCSO_CategoryId",
                principalTable: "SOCSO_Categories",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Payslip_Staffs_StaffId",
                table: "Payslip",
                column: "StaffId",
                principalTable: "Staffs",
                principalColumn: "Id");
        }
    }
}
