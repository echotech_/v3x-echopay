﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EchoPay.Migrations
{
    public partial class newfieldsinpayslip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "NetSalary",
                table: "Payslips",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "VariableDeduction",
                table: "Payslips",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "VariablePay",
                table: "Payslips",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NetSalary",
                table: "Payslips");

            migrationBuilder.DropColumn(
                name: "VariableDeduction",
                table: "Payslips");

            migrationBuilder.DropColumn(
                name: "VariablePay",
                table: "Payslips");
        }
    }
}
