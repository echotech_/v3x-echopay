﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EchoPay.Migrations
{
    public partial class changepayslipitem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Direction",
                table: "PayslipItems");

            migrationBuilder.DropColumn(
                name: "EPF_Applicable",
                table: "PayslipItems");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "PayslipItems");

            migrationBuilder.DropColumn(
                name: "SOCSO_Applicable",
                table: "PayslipItems");

            migrationBuilder.AddColumn<int>(
                name: "PresetPayslipItemId",
                table: "PayslipItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_PayslipItems_PresetPayslipItemId",
                table: "PayslipItems",
                column: "PresetPayslipItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_PayslipItems_PresetPayslipItems_PresetPayslipItemId",
                table: "PayslipItems",
                column: "PresetPayslipItemId",
                principalTable: "PresetPayslipItems",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PayslipItems_PresetPayslipItems_PresetPayslipItemId",
                table: "PayslipItems");

            migrationBuilder.DropIndex(
                name: "IX_PayslipItems_PresetPayslipItemId",
                table: "PayslipItems");

            migrationBuilder.DropColumn(
                name: "PresetPayslipItemId",
                table: "PayslipItems");

            migrationBuilder.AddColumn<int>(
                name: "Direction",
                table: "PayslipItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "EPF_Applicable",
                table: "PayslipItems",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "PayslipItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SOCSO_Applicable",
                table: "PayslipItems",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
