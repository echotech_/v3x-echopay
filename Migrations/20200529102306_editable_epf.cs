﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EchoPay.Migrations
{
    public partial class editable_epf : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EPF_Rates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AgeFrom = table.Column<int>(nullable: false),
                    EPF_StatusId = table.Column<int>(nullable: false),
                    SalaryRangeFrom = table.Column<double>(nullable: false),
                    EmployerRate = table.Column<double>(nullable: false),
                    EmployeeRate = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EPF_Rates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EPF_Rates_EPF_Statuses_EPF_StatusId",
                        column: x => x.EPF_StatusId,
                        principalTable: "EPF_Statuses",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_EPF_Rates_EPF_StatusId",
                table: "EPF_Rates",
                column: "EPF_StatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EPF_Rates");
        }
    }
}
