﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EchoPay.Migrations
{
    public partial class addpresets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PresetPayslipItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    Direction = table.Column<int>(nullable: false),
                    EPF_Applicable = table.Column<bool>(nullable: false),
                    SOCSO_Applicable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PresetPayslipItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PresetPayslipItems");
        }
    }
}
